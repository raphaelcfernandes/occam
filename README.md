# Occam

Occam is an archival toolchain designed to support digital preservation of computational artifacts.

## Installing

Using your operating system's package manager, install the following dependencies. Install `git`, `unzip`, `sqlite3`, and `python` 3.4 or above along with `pip`. You'll want to have some backend to run objects, so install and setup `docker`. Note: that the user that runs Occam needs to be able to run Docker which usually means putting that user in the `docker` group.

Now, with the prerequisites out of the way, get the code:

```
git clone https://gitlab.com/occam-archive/occam occam
```

Which will create a directory called `occam` which we can then go into to set up the rest of the system:

```
cd occam
pip install -r dev-requirements.txt
./bin/occam system initialize
```

The command "system initialize" will create a database. At this point you can run occam using:

```
./bin/occam
```

While will show the usage information. And you can use the following (while in this directory) to place occam in your path:

```
export PATH=$PATH:$PWD/bin
```

And to add this to your startup (again, while in the occam directory):

```
echo "export PATH=\$PATH:$PWD/bin" >> ~/.profile
```

The first thing you'll want to do is to create an account. The first account created on the system is automatically an administrator account. Replace `<username>` with the username you wish. It will ask for a password which will be used later to authenticate this account.

```
occam accounts new <username>
```

Continue by installing a client, such as the web-client [here](https://gitlab.com/occam-archive/occam-web-client).

These clients generally use Occam as a daemon and interact through this. To start a daemon:

```
occam daemon start
```

It will run in the background and can be closed using:

```
occam daemon stop
```

## Usage

For a listing of command line options, type:

```
./bin/occam
```

## Project Layout

```
docs                 - code documentation
src                  - root for all code
|- occam.py          - main application entrypoint
\- occam             - application code
   |- accounts       - accounts component    (handles account generation)
   |- backends       - backends component    (handles VM plugins)
   |- caches         - caches component      (manages cache services)
   |- commands       - commands component    (organizes cli commands)
   |- configurations - config component      (handles configuration objects)
   |- daemon         - daemon component      (daemon service)
   |- databases      - database component    (handles db access)
   |- jobs           - jobs component        (handles running tasks)
   |- links          - links component       (manages links to objects)
   |- manifests      - manifests component   (generates tasks)
   |- network        - network component     (world network access)
   |- nodes          - nodes component       (organizes known nodes)
   |- notes          - notes component       (manages metadata)
   |- objects        - objects component     (manages objects)
   |- permissions    - permissions component (manages access control)
   |- resources      - resources component   (stores/retrieves data)
   |- storage        - storage component     (handles data storage)
   |- system         - system component      (general system maintenance)
   \- workflows      - workflows component   (manages workflows)
```

## Code Documentation

We use Sphinx to parse documentation from the code into a set of HTML pages. To generate the code documentation, navigate to the docs directory and run:

```
make html
```

This will produce a website in the `docs/html` directory. Running a web server here using:

```
python3 -m http.server 8000
```

Will make the documentation site available at [http://localhost:8000](http://localhost:8000).

## Contributing

The following are accepted and cherished forms of contribution:

* Filing a bug issue. (We like to see feature requests and just normal bugs)
* Fixing a bug. (It's obviously helpful!)
* Adding documentation. (Help us with our docs or send us a link to your blog post!)
* Adding features.
* Adding artwork. (Art is the true visual form of professionalism)

The following are a bit harder to really accept, in spite of the obvious effort that may go into them, so please avoid this:

* Changing all of the javascript to coffeescript because it is "better"
* Rewriting all of the sass to whatever is newer. (It's happened to me before)
* Porting everything to rails.
* Creating a pull request with a "better" software license.

In general, contributions are easily provided by doing one of the following:

* Fork and clone the project.
* Update the code on your end however you see fit.
* Push that code to a public server.
* Create a pull request from your copy to ours.

The above is the most convenient process. You may create an issue with a link to a repository or tar/zip containing your code or patches as well, if git is not your thing.

## Acknowledgements

All attribution and crediting for contributors is located within [occam-web-client](https://gitlab.com/occam-archive/occam-web-client/blob/master/views/static/credits.md) or by visiting `/acknowledgements` in any Occam website.

## Code of Conduct

Please note that this project is released with a [Contributor Code of Conduct](CODE_OF_CONDUCT.md). By participating in this project you agree to abide by its terms.

## License

Occam is licensed under the AGPL 3.0. Refer to the [LICENSE.txt](LICENSE.txt) file in the root of the repository for specific details.
