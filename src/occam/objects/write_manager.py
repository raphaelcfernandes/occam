# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log     import loggable
from occam.manager import manager, uses
from occam.object  import Object

import os

from occam.objects.manager         import ObjectManager, ObjectError
from occam.nodes.manager           import NodeManager
from occam.notes.manager           import NoteManager
from occam.storage.manager         import StorageManager
from occam.resources.write_manager import ResourceWriteManager
from occam.backends.manager        import BackendManager
from occam.permissions.manager     import PermissionManager

@loggable
@manager("objects.write", reader=ObjectManager)
@uses(ObjectManager)
@uses(NodeManager)
@uses(NoteManager)
@uses(StorageManager)
@uses(BackendManager)
@uses(ResourceWriteManager)
@uses(PermissionManager)
class ObjectWriteManager:
  """ This manages the storing and updating of objects in the object store.
  """

  def store(self, obj, public=True):
    """ Retains a copy of the object in occam object pool.
    
    Local objects are on
    disk and can be altered at will by anybody. These objects are the actual
    persisted artifacts.
    """

    info = self.objects.infoFor(obj)
    revision = obj.head()
    obj.revision = revision
    object_type = info['type']

    if info.get('storable') == False:
      return

    # Place it in: .occam/objects/2-code/2-code/full-code
    uuid = info.get('id')

    storagePath = None
    sourcePath  = None
    if not obj.path is None:
      storagePath = self.storage.repositoryPathFor(uuid, revision, create=True)
      sourcePath  = obj.path

    if sourcePath != storagePath:
      # Otherwise, someone is attempting to store an object from the object store!
      # Thus, it is already stored... do nothing. report success.

      from occam.git_repository import GitRepository

      if GitRepository.isAt(storagePath):
        if GitRepository.hasRevision(storagePath, revision):
          ObjectWriteManager.Log.noisy("Version already exists in the object store")
        else:
          ObjectWriteManager.Log.noisy("Pushing new version at %s to %s" % (sourcePath, storagePath))

          git = GitRepository(storagePath)

          from uuid import uuid4
          remote_name = str(uuid4())
          new_branch_name = str(uuid4())

          ObjectWriteManager.Log.noisy("adding remote %s -> %s" % (remote_name, sourcePath))
          git.addRemote(remote_name, sourcePath)
          git.fetchRemoteBranches(remote_name)
          git.checkoutBranch(new_branch_name, GitRepository(sourcePath).branch(), remote_name)
          git.rmRemote(remote_name)
      else:
        ObjectWriteManager.Log.noisy("Cloning object repository at %s to %s" % (sourcePath, storagePath))
        GitRepository(sourcePath).clone(storagePath)

    # Commit storage
    # TODO: call upon storage manager to push to other stores
    #       this may be something we want to only sometimes do
    #       for instance IPFS publishes...
    self.storage.push(uuid, revision, storagePath)

    # Add Database Entry
    db_obj = self.updateDatabase(uuid=uuid, revision=revision, objectInfo=info)

    return db_obj

  def commit(self, obj, message="OCCAM automated commit", filepath="-a"):
    """ Commits the current state of the Object on disk with the given commit
    message.
    """

    from occam.git_repository import GitRepository

    info = self.objects.infoFor(obj)
    revision = None
    if obj.path:
      revision = GitRepository(obj.path).commit(message, filepath)

    ret = [revision]

    # Update the folks
    refs = self.updateParents(obj)
    for ref in refs:
      ret.append(ref)

    return ret

  def clone(self, obj, to=None, info=None, uuid=None, person=None):
    """ Creates an updated clone of the object.

    If no "path" is given, it will be performed in a temporary path. Otherwise,
    the object will be placed in that path.

    When "to" is specified, the object will be linked within the object given
    by "to" in the 'contains' category. The "belongsTo" will be set in the
    cloned object.

    The "info" parameter allows further modifications to be made to the object
    metadata of the new object.

    The "person" parameter gives the Person that is authorizing the clone. If
    this person does not have the current permission, the clone will fail.

    Returns: If the clone fails, this function will return None. Otherwise it
    returns an Object pointing to the newly created clone.
    """

    temporary = False

    person_uuid = None
    if person:
      person_uuid = person.uuid

    canClone = False

    if person and ('administrator' in person.roles):
      canClone = True

    if not canClone and not self.permissions.can("clone", uuid=self.objects.infoFor(obj).get('id'), person_uuid = person_uuid):
      # Not allowed to clone this object
      return None

    old_info = self.objects.infoFor(obj)

    tempToObject = None
    tempPath = None
    if to:
      # Create a temporary path for the "to" object
      root, tempToObject, tempPath = self.objects.temporaryClone(to, person = person)
      tempPath = os.path.join(tempPath, "%s-%s" % (Object.slugFor(old_info.get('type', 'object')), Object.slugFor(old_info.get('name', 'unknown'))))

      # Since this is temporary clone, the object will clean up itself
      # (although a linked copy may linger and be recloned under a different directory)
    else:
      # Create temporary path
      import tempfile
      tempPath = os.path.realpath(tempfile.mkdtemp(prefix="occam-"))
      temporary = True

      # Ensure it is an absolute path
      tempPath = os.path.realpath(tempPath)

    # Clone to this path
    new_obj = self.objects.clone(obj, path = tempPath)

    # Gather and update metadata
    new_info = self.objects.infoFor(new_obj)
    old_id = old_info['id']

    if uuid is None:
      from uuid import uuid1
      uuid = str(uuid1())

    new_info['id'] = uuid
    new_info['clonedFrom'] = {
      "type":     old_info['type'],
      "name":     old_info['name'],
      "id":       old_info['id'],
      "revision": obj.revision
    }

    # If this is being added to an object, update that metadata
    if to:
      to_info = self.objects.infoFor(to)

      new_info["belongsTo"] = {
        "id":   to_info.get('id'),
        "name": to_info.get('name', 'unknown'),
        "type": to_info.get('type', 'object')
      }

    #if cloneResources:
    #  resourceInfo = self.resources.cloneAll(obj)
    #  new_info['install'] = resourceInfo

    # Add other properties requested
    if info:
      new_info.update(info)

    # Store object
    self.updateObject(new_obj, new_info, 'cloned from %s; new id %s' % (old_id, new_info['id']))

    # Add this object to the requested parent
    if to:
      revisions, position = self.addDependency(tempToObject, object_type = new_info.get('type'),
                                                             name        = new_info.get('name'),
                                                             uuid        = new_info.get('id'),
                                                             revision    = new_obj.revision,
                                                             category    = 'contains')

      roots = (tempToObject.roots or [])[:]
      roots.append(tempToObject)
      new_obj = Object(uuid=uuid, path=new_obj.path, root=tempToObject.root, roots=roots, position=position)
    else:
      new_obj = Object(uuid=uuid, path=new_obj.path, root=new_obj.root, roots=new_obj.roots, position=new_obj.position)

    return new_obj

  def create(self, name, object_type, subtype = None, uuid = None, info = None, belongsTo = None, root = None, path = None, source = None, createPath = True):
    """ Creates a new object with the given properties.
    """

    import json

    # Generate a new UUID
    if uuid is None:
      from uuid import uuid1
      uuid = str(uuid1())

    # Make sure the incoming metadata is at least a dict
    if info is None:
      info = {}

    # Craft the object metadata
    object_json = info.copy()
    object_json.update({
      "name": name,
      "type": object_type,
      "id":   uuid,
    })

    # Add root field. The 'workset', for instance, is the root object for knowing
    # the access privilege of the object.
    if root:
      object_json['root'] = {
        "type": root.get('type'),
        "id":   root.get('id'),
        "name": root.get('name')
      }

    # Add source
    if source:
      object_json['source'] = source

    # Add subtype
    if subtype:
      object_json['subtype'] = subtype

    # Add belongsTo field. This determines the object that contains this one.
    if belongsTo:
      object_json['belongsTo'] = {
        "type": belongsTo.get('type'),
        "id":   belongsTo.get('id'),
        "name": belongsTo.get('name')
      }

    # Create directory, if it doesn't exist, warn if it does
    if path:
      if os.path.exists(path):
        if createPath:
          ObjectWriteManager.Log.warning("directory %s already exists" % (path))
      else:
        ObjectWriteManager.Log.noisy("creating directory %s" % (path))
        os.mkdir(path)
    else:
      # Create in the object store
      path = self.storage.repositoryPathFor(uuid, revision=None, create=True) 

    # Create object metadata
    object_json_path = os.path.join(path, "object.json")

    with open(object_json_path, "w") as f:
      f.write(json.dumps(object_json, indent=2, separators=(',', ': ')))

    # Create .gitignore
    with open("%s/.gitignore" % (path), 'w+') as f:
      pass

    # Create git repository in group path
    from occam.git_repository import GitRepository
    git = GitRepository.create(path)

    # Initial commit to get a revision
    git.add('*')
    git.commit("Created %s %s" % (object_type, name))

    # Pull revision
    revision = git.head()
    ObjectWriteManager.Log.noisy("Committed to repository as %s" % (revision))

    return Object(uuid=uuid, path=path, root=root, belongsTo=belongsTo)

  def updateData(self, obj, path, data, message):
    """ Rewrites the given file in the given object with the given data.
    """

    info = self.objects.infoFor(obj)

    if(path.startswith('/')):
        path=path[1:]
    with open(os.path.join(obj.path, path), 'wb+') as f:
      f.write(data)

    # Add this file to the git index
    if obj.path:
      from occam.git_repository import GitRepository
      GitRepository(obj.path).add(path)

    # Commit this file
    if message is None:
      message = "Updating data file."
    new_revisions = self.commit(obj, message)

    # Update normalized view
    obj.revision = new_revisions[0]
    self.updateDatabase(uuid       = info.get('id'),
                        revision   = obj.revision,
                        objectInfo = info)

    return new_revisions

  def updateObjectJSON(self, obj, path, data, message):
    """ Rewrites the given JSON file in the given object with the given data.
    """

    # Write out the new object metadata
    import json

    info = self.objects.infoFor(obj)

    if(path.startswith('/')):
        path=path[1:]
    with open(os.path.join(obj.path, path), 'w+') as f:
      f.write(json.dumps(data, indent=2, separators=(',', ': ')))

    # Add this file to the git index
    if obj.path:
      from occam.git_repository import GitRepository
      GitRepository(obj.path).add(path)

    # Commit this file
    if message is None:
      message = "Updating json file."
    new_revisions = self.commit(obj, message)

    # Update normalized view
    obj.revision = new_revisions[0]
    self.updateDatabase(uuid       = info.get('id'),
                        revision   = obj.revision,
                        objectInfo = info)

    return new_revisions

  def updateObject(self, obj, info, message):
    """ Rewrites the object.json for the given object with the given data.

    This method will replace the object metadata with the given metadata. It
    will commit that change in the log with the given message. It will update
    the database normalization as well.
    """

    # Write out the new object metadata
    import json

    with open(os.path.join(obj.path, "object.json"), 'w+') as f:
      f.write(json.dumps(info, indent=2, separators=(',', ': ')))

    # Update our view of ourselves
    obj.info = info

    new_revisions = self.commit(obj, message, 'object.json')

    links = self.objects.links.retrieveLocalLinks(obj, path = obj.path)
    for link in links:
      # Update local link!
      self.objects.links.updateLocalLink(link, newRevision = new_revisions[0])

    # Update normalized view
    obj.revision = new_revisions[0]
    obj.infoRevision = new_revisions[0]
    self.updateDatabase(uuid       = info.get('id'),
                        revision   = obj.revision,
                        objectInfo = info)

    # Store
    self.store(obj)

    return new_revisions

  def updateParents(self, obj):
    info = self.objects.infoFor(obj)

    ret = []

    # Update parent
    parent = self.objects.parentFor(obj)
    if parent:
      parent_revision, _ = self.updateDependency(parent, info['id'], info['type'], info['name'], obj.head(), position = obj.position, category="contains")
      self.store(parent)
      for ref in parent_revision:
        ret.append(ref)

    return ret

  def updateDependency(self, obj, uuid, object_type=None, name=None, revision=None, path=None, position=None, category='dependencies'):
    info = self.objects.infoFor(obj)
    info[category] = info.get(category, [])

    ret = None
    if position:
      ret = info[category][position]
    else:
      i = 0
      for dependency in info[category]:
        if dependency['id'] == uuid:
          position = i
          ret = dependency
          break
        i = i + 1

    if ret:
      if name:
        ret['name'] = name
      if revision:
        ret['revision'] = revision
      if object_type:
        ret['type'] = object_type
      if path:
        ret['local'] = True
        ret['path'] = path
      else:
        if 'local' in ret:
          del ret['local']

    return self.updateObject(obj, info, 'updates dependency %s %s' % (object_type, name)), position

  def addDependency(self, obj, uuid, object_type, name, revision, path=None, category='dependencies'):
    info = self.objects.infoFor(obj)
    info[category] = info.get(category, [])

    ret = {
      'name':     name,
      'type':     object_type,
      'id':       uuid,
      'revision': revision
    }

    if not path is None:
      ret['local'] = True
      ret['path'] = path

    position = len(info[category])
    info[category].append(ret)

    return self.updateObject(obj, info, 'adds %s %s as a dependency' % (object_type, name)), position

  def purgeGenerates(self, obj):
    info = self.objects.infoFor(obj)

    info['generates'] = []
    self.updateObject(obj, info, 'Clearing any generated objects.')

    return True

  def addFileTo(self, obj, filename, data):
    """ Adds the given file data to the requested filename.
    """

    filepath = os.path.realpath(os.path.join(obj.path, filename))

    if isinstance(data, str):
      with open(filepath, "w+") as f:
        f.write(data)
    else:
      with open(filepath, "wb+") as f:
        f.write(data)

    from occam.git_repository import GitRepository
    git = GitRepository(obj.path)
    git.add(filename)

  def removeObjectFrom(self, obj, subObject, revision = None):
    """ Removes the given subObject from the given object's "contains" section.

    Returns:
      (Object) The revised object.
    """

    info = self.objects.infoFor(obj)

    subIndex = self.objects.objectPositionWithin(obj, subObject, revision = revision)

    if subIndex is not None:
      del info['contains'][subIndex]
      self.updateObject(obj, info, "Removed object.")

    return obj

  def addObjectTo(self, obj, name=None, object_type=None, path=None, info=None, create=True, subObject = None):
    """ Creates a new object on disk.

    Returns:
      (Object) The created object.
    """

    if subObject:
      if not name:
        name = self.objects.infoFor(subObject).get('name')

      if not object_type:
        object_type = self.objects.infoFor(subObject).get('type')

    if name == "" or name is None:
      # Do not allow an empty name
      raise ObjectCreateError("name cannot be blank")

    if object_type == "" or object_type is None:
      # Do not allow an empty type
      raise ObjectCreateError("object type cannot be blank")

    # Create directory, if it doesn't exist, warn if it does
    if path is None:
      from occam.object import Object

      slug = "%s-%s" % (Object.slugFor(object_type), Object.slugFor(name))
      path = os.path.realpath(os.path.join(obj.path, slug))

    rootInfo = None
    if obj.root:
      rootInfo = self.objects.infoFor(obj.root)

    if create:
      subObject = self.create(path=path, name=name, object_type=object_type, belongsTo=self.objects.infoFor(obj), root=rootInfo, info=info)

      self.store(subObject)

      # Update base object to ignore object in repository
      f = open('%s/.gitignore' % (obj.path), 'a+')
      f.write('%s\n' % (slug))
      f.close()

    revision = subObject.head()
    uuid = self.objects.infoFor(subObject)['id']
    position = None

    revisions, position = self.addDependency(obj,
                                             object_type = object_type,
                                             name        = name,
                                             revision    = revision,
                                             uuid        = uuid,
                                             category    = 'contains')

    roots = (obj.roots or [])[:]
    roots.append(obj)
    return Object(path=subObject.path, root=self.objects.infoFor(obj), roots=roots, position = position)

  def createResourceObject(self, resourceType, uuid, name, source):
    """ Creates a resource object
    """

    ObjectWriteManager.Log.write("Creating Object for resource %s" % (uuid))
    new_obj = self.create(name        = name,
                          uuid        = uuid,
                          source      = source,
                          object_type = "resource",
                          subtype     = resourceType,
                          info = {
                            "resource": resourceType,
                          })

    self.store(new_obj)

    return new_obj

  def pullResources(self, obj):
    """ Installs the listed linked resources for the given object to the given path.

    Installs the listed resources in the Object's "install" section to the
    given path or the local path of the Object if no path is given. Will
    install or discover resources as needed.
    """

    object_info = self.objects.infoFor(obj)

    save_object_info = False
    for section in [None, "build", "run"]:
      section_info = object_info
      if section is not None:
        section_info = object_info.get(section)

      if section_info is None:
        continue

      resources = self.resources.write.pullAll(section_info, rootPath=obj.path)

      section_info['install'] = section_info.get('install', [])

      repositories = section_info['install']

      for resourceInfo, info in zip(resources, repositories):
        if resourceInfo.get('info') is None:
          continue

        uuid     = resourceInfo.get('info').get('id')
        revision = resourceInfo.get('info').get('revision')
        install  = resourceInfo.get('info').get('install')

        # Apply id/revision
        if info.get('id') != uuid or info.get('revision') != revision or info.get('install') != install:
          info['id'] = uuid
          info['revision'] = revision
          if install:
            info['install'] = install
          save_object_info = True

        # Look for object record
        db_rows = self.objects.search(uuid=uuid)
        db_obj  = None
        if db_rows:
          db_obj = db_rows[0]

        # If no object record for this resource, create it
        if db_obj is None:
          sub_obj = self.createResourceObject(resourceType = info.get('type', 'file'),
                                              uuid         = uuid,
                                              name         = info.get('name', 'resource'),
                                              source       = info.get('source'))
          self.store(sub_obj)

          db_rows = self.objects.search(uuid=uuid)
          if len(db_rows) > 0:
            db_obj = db_rows[0]


        # Look for the resource record
        db_resource = self.resources.retrieveResource(resourceType = info.get('type', 'file'),
                                                      uuid         = uuid,
                                                      revision     = info.get('revision'),
                                                      source       = info.get('source'))

        if db_resource is None:
          self.resources.write.createResource(resourceType = info.get('type', 'file'),
                                              objectRecord = db_obj,
                                              revision     = info['revision'],
                                              source       = info.get('source'))

      # New resources
      if len(resources) > len(repositories):
        ObjectManager.Log.write("Discovered new resources")
        for resourceInfo in resources[len(repositories):]:
          ObjectManager.Log.write("Found new resource: %s" % (resourceInfo.get('name')))
          repositories.append(resourceInfo)
          save_object_info = True

    if save_object_info:
      message = "Updated object's install repositories id/revisions."
      if len(object_info['install']) == 0:
        del object_info['install']
      self.updateObject(obj, object_info, message)

    return resources

  def pullSubObjects(self, obj):
    """ Looks at the included objects and gives them ids if they need them.

    Returns True if the object was updated.
    """

    object_info = self.objects.infoFor(obj)

    save_object_info = False

    for subObject in object_info.get('includes', []):
      # New resources
      if subObject.get('id') is None or subObject.get('id') == "":
        ObjectManager.Log.write("Discovered new subobject %s %s" % (subObject.get('type', 'object'), subObject.get('name', 'unknown')))

        from uuid import uuid1
        uuid = str(uuid1())

        subObject['id'] = uuid
        save_object_info = True

    if save_object_info:
      message = "Updated object's included objects"
      self.updateObject(obj, object_info, message)

    return save_object_info

  def addAuthorTo(self, obj, person):
    """ Add author to an object's metadata.

    This will not add an Authorship record. This is done by the AccountManager.
    """

    info = self.objects.infoFor(obj)

    uuid = self.objects.infoFor(person)['id']
    name = self.objects.infoFor(person)['name']

    info['authors'] = (info.get('authors') or [])
    if not uuid in info['authors']:
      info['authors'].append(uuid)
    else:
      Log.warning("%s is already an author in %s" % (name, info['name']))
      return False

    # TODO: handle revisions?

    self.updateObject(obj, info, "adds %s as author" % (name))

    return True

  def addCollaboratorTo(self, obj, person):
    """ Add collaborator to an object.

    This will not add a Collaboratorship record. This is done by the AccountManager.
    """

    info = self.objectInfo()

    uuid = person.objectInfo()['id']
    name = person.objectInfo()['name']

    info['collaborators'] = (info.get('collaborators') or [])
    if not uuid in info['collaborators']:
      info['collaborators'].append(uuid)
    else:
      Log.warning("%s is already a collaborator in workset %s" % (name, info['name']))
      return False

    # TODO: handle revisions?

    self.updateObject(info, "adds %s as collaborator" % (name))

    return True

  def delete(self, obj):
    """ Deletes the given object.
    """

    uuid = obj.uuid
    self.notes.delete(uuid)

    ObjectManager.Log.write("Deleting content at %s" % (obj.path))

    # Remove local content
    path = self.storage.repositoryPathFor(uuid, revision=None, create=True) 
    if os.path.exists(path):
      import shutil
      shutil.rmtree(path)

    return self.datastore.delete(obj)

  def updateDatabase(self, uuid, revision, objectInfo, owner_db_obj=None):
    """ Updates or creates the ObjectRecord for the given object.

    Returns the ObjectRecord created or updated.
    """

    # Ensure the owner tag is set
    for derivative in objectInfo.get('includes', []):
      # Write owner tag
      self.notes.store(derivative.get('id'), "owner", key="id", value=uuid, revision=None)

    # Rake 'provides' and add Provider records:
    self.backends.store(uuid, revision, objectInfo)

    return self.datastore.updateDatabase(uuid, revision, objectInfo, owner_db_obj)

  def rebuild(self, person = None):
    """ Rebuilds information about objects stored in the object store.

    This can be called when the database is damaged or lost to rebuild the
    local knowledge of the objects on the system.
    """

    for uuid in self.notes.retrieveObjectList():
      # Determine if we know this object already
      obj = self.objects.retrieve(uuid = uuid, person = person)

      if obj is None:
        # Look for the object in storage
        repositoryPath = self.storage.repositoryPathFor(uuid, None)
        resourcePath   = self.storage.resourcePathFor(uuid, None)

        if resourcePath:
          ObjectWriteManager.Log.write("Storing resource %s" % (uuid))

          try:
            obj = Object(uuid = uuid, path = repositoryPath)
            info = self.objects.infoFor(obj)
          except:
            continue

          # Look for object record
          db_rows = self.objects.search(uuid=uuid)
          db_obj  = None
          if db_rows:
            db_obj = db_rows[0]

          # If no object record for this resource, create it
          if db_obj is None:
            sub_obj = self.createResourceObject(resourceType = info['type'],
                                                uuid         = uuid,
                                                name         = info.get('name', 'resource'),
                                                source       = info.get('source'))
            self.store(sub_obj)

            db_rows = self.objects.search(uuid=uuid)
            if len(db_rows) > 0:
              db_obj = db_rows[0]

          # Look for the resource record
          db_resource = self.resources.retrieveResource(resourceType = info.get('type'),
                                                        uuid         = uuid,
                                                        revision     = info.get('revision'),
                                                        source       = info.get('source'))

          if db_resource is None:
            self.resources.write.createResource(resourceType = info['type'],
                                                objectRecord = db_obj,
                                                revision     = info['revision'],
                                                source       = info.get('source'))


        elif repositoryPath:
          ObjectWriteManager.Log.write("Storing object %s" % (uuid))

          obj = Object(uuid = uuid, path = repositoryPath)
          info = self.objects.infoFor(obj)

          self.updateDatabase(uuid=uuid, revision=obj.revision, objectInfo=info)

class ObjectCreateError(ObjectError):
  """ Object creation error.
  """

  def __init__(self, message):
    """ Creates a new ObjectCreateError.
    """
    self.message = message

  def __str__(self):
    return self.message
