# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log              import Log
from occam.object           import Object
from occam.manager          import uses

from occam.objects.manager    import ObjectManager
from occam.manifests.manager  import ManifestManager

from occam.commands.manager   import command, option, argument

import json

@command('objects', 'viewers',
  category      = 'Running Objects',
  documentation = "Returns a list of objects that may be used for viewing the given object.")
@argument("object", type = "object", nargs = '?')
@option("-j", "--json",    dest    = "to_json",
                           action  = "store_true",
                           help    = "returns result as a json document")
@option("-s", "--subtype", dest    = "subtype",
                           action  = "append",
                           help    = "the subtype to query")
@option("-t", "--type",    dest    = "type",
                           action  = "append",
                           help    = "the type to query")
@uses(ObjectManager)
class ObjectViewersCommand:
  """ This class will handle gathering a list of viewer objects for the given object.

  Retrieve the possible viewer objects that can view this object:
  occam objects viewers 7a8de00a-16f0-11e6-8bfb-001fd05bb228@95ada4dd719f4de146c66871ede425201821ca50
  """

  def do(self, recursive=False):
    obj = self.objects.resolve(self.options.object, person = self.person)

    if obj is None:
      if self.options.object:
        Log.error("Could not find the given object.")
      else:
        Log.error("Could not find a valid object in the current directory.")
      return -1

    info = None
    subtypes = None

    if self.options.subtype:
      subtypes = self.options.subtype
    else:
      if info is None:
        info = self.objects.infoFor(obj)
      subtypes = info.get('subtype')

    if subtypes is None:
      subtypes = []

    if not isinstance(subtypes, list):
      subtypes = [subtypes]

    type = None
    if self.options.type:
      type = self.options.type[0]
    else:
      if info is None:
        info = self.objects.infoFor(obj)
      type = info.get('type')

    # We will return this array of objects
    viewers = []

    # First get the viewers that are very specific
    for subtype in subtypes:
      viewers.extend(self.objects.viewersFor(viewsType    = type,
                                             viewsSubtype = subtype))

    # Get the generic viewers
    viewers.extend(self.objects.viewersFor(viewsType = type))

    for subtype in subtypes:
      viewers.extend(self.objects.viewersFor(viewsSubtype = subtype))

    if self.options.to_json:
      ret = [{
        "id":           obj.uid,
        "revision":     obj.revision,
        "name":         obj.name,
        "description":  obj.description,
        "type":         obj.object_type,
        "subtype":      obj.subtype,
        "architecture": obj.architecture,
        "organization": obj.organization,
        "environment":  obj.environment,
      } for obj in viewers]
      Log.output(json.dumps(ret))
    else:
      for viewer in viewers:
        Log.output(viewer.name + " - " + viewer.uid + "@" + viewer.revision)

    return 0
