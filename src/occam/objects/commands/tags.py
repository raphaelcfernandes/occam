# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log    import Log
from occam.object import Object

from occam.commands.manager import command, option, argument

from occam.objects.manager import ObjectManager
from occam.notes.manager   import NoteManager

from occam.manager import uses

@command('objects', 'tags')
@argument("object", type="object", nargs="?")
@option("-j", "--json", dest   = "to_json",
                        action = "store_true",
                        help   = "returns result as a json document")
@uses(ObjectManager)
@uses(NoteManager)
class TagsCommand:
  def do(self):
    if self.options.object is None:
      obj = Object(path=".")
    else:
      obj = self.objects.resolve(self.options.object,
                                 person = self.person)

    Log.header("Listing tags")

    # List existing tags
    tagData = self.notes.retrieve(obj.uuid, "versions", revision="values") or {}

    if not tagData and not self.options.to_json:
      Log.write("No tags")

    tags = {}

    for k,v in tagData.items():
      tags[k] = [item['value'] for item in v]

    if self.options.to_json:
      import json
      Log.output(json.dumps(tags))
    else:
      for k,v in tags.items():
        Log.write("%s: %s" % (k, " ".join(v)))

    return 0
