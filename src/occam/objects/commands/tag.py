# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log    import Log
from occam.object import Object

from occam.commands.manager import command, option, argument

from occam.objects.manager import ObjectManager
from occam.notes.manager   import NoteManager

from occam.manager import uses

@command('objects', 'tag')
@argument("object", type="object", nargs="?")
@argument("version")
@option("--overwrite", dest   = "overwrite",
                       action = "store_true",
                       help   = "forces the adding of the tag even if it already exists")
@option("-j", "--json", dest   = "to_json",
                        action = "store_true",
                        help   = "returns result as a json document")
@uses(ObjectManager)
@uses(NoteManager)
class TagCommand:
  def do(self):
    if self.options.object is None:
      obj = Object(path=".")
    else:
      obj = self.objects.resolve(self.options.object,
                                 person = self.person)

    Log.header("Applying tag")

    if obj is None:
      Log.error("Could not find the object to tag.")
      return -1

    # Look at whether or not the tag is already stored
    revisions = self.notes.retrieveValues(uuid     = obj.uuid,
                                          category = "versions",
                                          key      = self.options.version,
                                          revision = None)

    if revisions:
      if obj.revision not in revisions:
        if not self.options.overwrite:
          Log.error("The requested tag is already being used. Use --overwrite to add this tag.")
          return -1
        else:
          Log.warning("Tag already exists and is being appended.")
      else:
        Log.warning("The tag is already set to this revision.")

    # Tag the version in the note store
    self.notes.store(uuid     = obj.uuid,
                     category = "versions",
                     key      = self.options.version,
                     value    = obj.revision,
                     revision = None)

    # Success
    Log.write("Version %s applied to revision %s" % (self.options.version, obj.revision))
    return 0
