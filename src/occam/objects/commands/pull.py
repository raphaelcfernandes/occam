# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.object         import Object
from occam.object_info    import ObjectInfo

from occam.log import Log

from occam.manager import uses

from occam.network.manager       import NetworkManager
from occam.nodes.manager         import NodeManager
from occam.backends.manager      import BackendManager
from occam.objects.write_manager import ObjectWriteManager
from occam.objects.manager       import ObjectJSONError
from occam.permissions.manager   import PermissionManager

from occam.commands.manager import command, option, argument

@command('objects', 'pull',
  category      = "Object Discovery",
  documentation = "Pulls an object from a specific URL or path.")
@argument("object", action = "store", type="object", default=".", nargs="?")
@option("-t", "--task",      action = "store_true",
                             dest   = "pull_task",
                             help   = "will pull any necessary objects needed to run this object")
@uses(NetworkManager)
@uses(NodeManager)
@uses(BackendManager)
@uses(PermissionManager)
@uses(ObjectWriteManager)
class PullCommand:
  def do(self):
    Log.header("Pulling object")

    git_path = None

    # Grab the object from the command line argument
    try:
      obj = self.objects.resolve(self.options.object, person = self.person, allowNone = True)
    except ObjectJSONError as e:
      Log.error("Invalid object.json")
      Log.write(e.report)
      Log.output(e.context, padding="")
      return -1

    if obj is None:
      Log.error("Object not found")
      return -1

    # Pull any objects determined by other nodes to be necessary to build/run
    # this one
    if self.options.pull_task:
      # TODO: update for more than one backend
      docker_backend = self.backends.handlerFor('docker')
      backend = docker_backend.provides()[0]
      objInfo = obj.objectInfo();
      hypotheticalTask = self.nodes.taskFor(backend[0], backend[1],
                           objInfo.get('environment'),
                           objInfo.get('architecture'))
      objs = self.occam.taskObjectsFor(hypotheticalTask)
      for discoveredObj in objs:
        Log.write("Discovered object %s %s" % (discoveredObj.get('type'),
                                               discoveredObj.get('name')))

        # Pull this object
        self.nodes.pullAllObjects(discoveredObj.get('id'))

    # Discover the resources
    resources = self.objects.write.pullResources(obj)

    # Ensure subobjects are formed correctly and assigned a UUID
    updated = self.objects.write.pullSubObjects(obj)

    # Store the object
    self.objects.write.store(obj)

    objectInfo = self.objects.infoFor(obj)
    self.permissions.update(uuid = objectInfo.get('id'), canRead=True, canWrite=True, canClone=True)

    Log.done("discovered %s %s" % (objectInfo.get('type'), objectInfo.get('name')))
    return 0
