# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.commands.manager import command, option, argument

from occam.object import Object
from occam.log    import Log

from occam.manager import uses

from occam.objects.manager    import ObjectManager

@command('objects', 'history',
  category      = 'Object Inspection',
  documentation = "Displays the revision history for the object.")
@argument("object", type = "object", help = "The object ID to view the history of.")
@option("-j", "--json", dest   = "to_json",
                        action = "store_true",
                        help   = "returns result as a json document")
@uses(ObjectManager)
class HistoryCommand:
  def do(self):
    # Get the object to list
    obj = None

    if self.options.object is None:
      # Use the object in the current directory if <object> argument is omitted
      obj = Object(path='.')
    elif self.options.object.id:
      # Query for object by id
      obj = self.objects.retrieve(uuid=self.options.object.id, object_options=self.options.object, person = self.person)
      if obj is None:
        Log.error("cannot find object with id %s" % (self.options.object.id))
        return -1

    try:
      data = self.objects.retrieveHistoryFor(obj)
    except IOError:
      data = []

    if self.options.to_json:
      import json
      Log.output(json.dumps(data))

      return 0

    Log.output(data)
    return 0
