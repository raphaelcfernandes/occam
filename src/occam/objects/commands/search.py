# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log              import Log
from occam.manager          import uses

from occam.commands.manager import command, option

from occam.objects.manager  import ObjectManager

@command('objects', 'search',
  category      = "Object Discovery",
  documentation = "Look up object details only from our local knowledge.")
@option("-t", "--type", action = "store",
                        dest   = "object_type",
                        help   = "search for objects with the specified type")
@option("-n", "--name", action = "store",
                        dest   = "name",
                        help   = "search for objects with the specified name")
@option("-u", "--uuid", action = "store",
                        dest   = "uuid",
                        help   = "search for the object with the specified uuid")
@option("-j", "--json", dest   = "to_json",
                        action = "store_true",
                        help   = "returns result as a json document")
@option(      "--types", dest  = "return_types",
                        action = "store",
                        type   = str,
                        help   = "return only types matching the given string")
@uses(ObjectManager)
class SearchCommand:
  def do(self):
    if not self.options.return_types is None:
      Log.header("Listing Found Object Types")

      types = self.objects.searchTypes(query = self.options.return_types)

      if self.options.to_json:
        import json
        Log.output(json.dumps(types))
      else:
        for object_type in types:
          Log.output(object_type)
    else:
      objs = self.objects.search(object_type = self.options.object_type,
                                 name        = self.options.name,
                                 uuid        = self.options.uuid)

      Log.header("Listing Found Objects")

      if self.options.to_json:
        ret = [{"id":           obj.uid,
                "revision":     obj.revision,
                "name":         obj.name,
                "description":  obj.description,
                "type":         obj.object_type,
                "subtype":      obj.subtype,
                "architecture": obj.architecture,
                "organization": obj.organization,
                "environment":  obj.environment,
               } for obj in objs]

        import json
        Log.output(json.dumps(ret))
      else:
        for obj in objs:
          Log.output(obj.name)
          Log.output(obj.uid, padding="    uuid: ")

      Log.write("found %s object(s)" % (len(objs)))

    return 0
