# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os, json

from occam.config     import Config
from occam.log        import loggable

from occam.manager import manager, uses

from occam.objects.write_manager  import ObjectWriteManager
from occam.manifests.manager      import ManifestManager
from occam.configurations.manager import ConfigurationManager
from occam.databases.manager      import DatabaseManager
from occam.jobs.manager           import JobManager

from occam.workflows.datatypes.workflow import Workflow
from occam.workflows.datatypes.workflow import Node as WorkflowNode
@loggable
@uses(ObjectWriteManager)
@uses(ManifestManager)
@uses(ConfigurationManager)
@uses(DatabaseManager)
@uses(JobManager)
@manager("workflows")
class WorkflowManager:
  """ This OCCAM manager handles workflows, configurations, and job generation.
  """
  def create_workflow(self, object, person=None):
    info = self.objects.infoFor(object)
    data = {}
    if 'file' in info:
      try:
        data = self.objects.retrieveJSONFrom(object, info.get('file'))
      except:
        pass
    return Workflow(info, data, object.revision)

  # We will turn a workflow into a set of workflows for each possible
  # configuration (taking into account ranges and the like)

  # We will then turn each of these into a set of tasks which can be queued
  # together

  def dataFor(self, workflowObject, person = None):
    """ Returns the data for the workflow.
    """

    workflow = self.create_workflow(workflowObject, person=person)
    return workflow.data

  def isNodeWorkflow(self, node, person = None):
    """ Returns True if the given node describes a workflow object.
    """
    return node.isType('workflow')

  def isNodeRunnable(self, node, person = None):
    """ Returns True if the given node describes a running object.
    """

    object = self.objects.retrieve(uuid     = node.uuid,
                                   revision = node.revision,
                                   version  = node.version,
                                   person   = person)
    info = self.objects.infoFor(object)

    # If this node is attached as input, then it does not run
    # It is input instead.

    # However, it may need to run its initial phase to determine
    # its command if it has a 'run.script' key and the wire
    # consuming it has an 'occam/runnable' tag.
    if len(node.self.get('connections', [])) > 0:
      if 'run' in info and 'script' in info['run']:
        return True
      return False

    info = self.objects.infoFor(object)
    return 'run' in info

  def jobsForNode(self, runId, nodeIndex):
    """ Returns a list of unique job ids pertaining to the given node.
    """

    import sql

    # Look up all run job records
    runJobs = sql.Table('run_jobs')

    session = self.database.session()

    query = runJobs.select(runJobs.job_id)
    query.where = (runJobs.run_id == runId) & \
                  (runJobs.node_index == nodeIndex)

    self.database.execute(session, query)
    rows = self.database.many(session, size=1000)

    return list(set([x["job_id"] for x in rows]))

  def generateRunForNode(self, workflow, info, run, person = None):
    """ Returns a list of objects and inputs to run for the given node.

    When withInput is specified, it indicates new data into an output wire.
    It will then use that data on the corresponding input wire(s) and permute
    the remaining wires.
    """

    # REMEMBER: if the same wire is connected to two or more inputs, it shares
    #           the data. It does not permute the possible data.

    ret = []

    node         = info.get('node')
    existingTask = info.get('task')
    taskPath     = info.get('path')

    # Get the node object
    object = self.objects.retrieve(uuid     = node.uuid,
                                   revision = node.revision,
                                   version  = node.version,
                                   person   = person)

    objectInfo = self.objects.infoFor(object)

    # Returns empty array when node is not runnable
    if not (self.isNodeRunnable(node, person=person) or self.isNodeWorkflow(node, person=person)):
      return ret

    nodeCanRun = True
    inputs = []
    for wireIndex, wire in enumerate(node.inputs):
      inputs.append([])

      # Look at configuration inputs and permute them when necessary to generate the
      # actual inputs for the run.
      if wire.get('type') == "configuration" and len(wire.get('connections', [])) == 0:
        # Permute default configurations

        # Add an imaginary source attached to this node
        inputs[-1].append([])

        schema = self.configurations.schemaFor(object, wireIndex)
        baseConfiguration = self.configurations.defaultsFor(schema)

        inputMetadata = objectInfo.get('inputs', [])[wireIndex]

        for configuration in self.configurations.permute({}, baseConfiguration):
          WorkflowManager.Log.write("Generated default configuration for %s for %s" % (wire.get('name'), node.name))
          # Generate the configuration object and store that as a possible input
          # We will permute all the configuration inputs when we build tasks

          # This would generate a uuid that hashes the configuration
          #import uuid
          #print(uuid.uuid5(uuid.NAMESPACE_URL, json.dumps(configuration, sort_keys=True)))

          # Create the configuration object
          configObject = self.objects.write.create(name="configuration", object_type="configuration", info = {
            "file": inputMetadata.get('file', 'config.json'),
            "schema": {
              "id": objectInfo.get('id'),
              "revision": object.revision,
              "file": inputMetadata.get('schema'),
              "type": "application/json",
              "name": objectInfo.get('name')
            }
          })

          # Store the generated configuration
          self.objects.write.addFileTo(configObject, inputMetadata.get('file', 'config.json'), json.dumps(configuration))
          self.objects.write.commit(configObject, message="Adds configuration")
          self.objects.write.store(configObject)

          # TODO: Make sure to tie the subset of updated parameters to the output

          # Add it is an input set for this wire
          inputs[-1][-1].append([configObject])

      # Otherwise, look at any other type of input
      else:
        print("resolving input")
        objects = self.resolveInputs(run, workflow, node, wireIndex, person)

        if len(objects) == 0 and len(wire.get('connections', [])) != 0:
          nodeCanRun = False

        for i, input_object in enumerate(objects):
          # Make room for the source
          inputs[-1].append([])

          if wire.get('type') == "configuration":
            # We need to append all permutations of this object

            schema = self.configurations.schemaFor(input_object)
            data = self.configurations.dataFor(input_object)
            baseConfiguration = self.configurations.defaultsFor(schema)
            subObjectInfo = self.objects.infoFor(input_object)

            inputMetadata = objectInfo.get('inputs', [])[wireIndex]
            ranges = self.configurations.rangedValuesFor(data, schema)
            configurationSpace = self.configurations.expandRangedValue(ranges)
            for configuration in self.configurations.permute(configurationSpace, baseConfiguration):
              WorkflowManager.Log.write("Generated configuration for %s for %s" % (wire.get('name'), node.get('name')))
              # Generate the configuration object and store that as a possible input
              # We will permute all the configuration inputs when we build tasks

              # This would generate a uuid that hashes the configuration
              #import uuid
              #print(uuid.uuid5(uuid.NAMESPACE_URL, json.dumps(configuration, sort_keys=True)))

              # Create the configuration object
              configObject = self.objects.write.create(name="configuration", object_type="configuration", info = {
                "file": inputMetadata.get('file', 'config.json'),
                "schema": {
                  "id": objectInfo.get('id'),
                  "revision": input_object.revision,
                  "file": inputMetadata.get('schema'),
                  "type": "application/json",
                  "name": objectInfo.get('name')
                },
                "generator": [{
                  "id": input_object.uuid,
                  "revision": input_object.revision,
                  "file": subObjectInfo.get('file'),
                  "name": subObjectInfo.get('name'),
                  "type": subObjectInfo.get('type'),
                }]
              })

              # Store the generated configuration
              self.objects.write.addFileTo(configObject, inputMetadata.get('file', 'config.json'), json.dumps(configuration))
              self.objects.write.commit(configObject, message="Adds configuration")
              self.objects.write.store(configObject)

              # TODO: Make sure to tie the subset of updated parameters to the output

              # Add it is an input set for this wire
              inputs[-1][-1].append([configObject])
          else: # Not configuration
            input_info = self.objects.infoFor(input_object)
            input_info['revision'] = input_object.revision

            # If this is a runnable self object, we need to include the run
            # report as the command.
            source = self.resolveInputSource(workflow, node, wireIndex, i)
            if input_info.get('run', {}).get('script') and self.isInputSourceSelf(workflow, node, wireIndex, i):
              # Get the job that ran this and determine its report
              jobs = self.jobsFor(run.id, nodeIndex = source.index).get('nodes', {}).get(source.index, {}).get('jobs', [])
              for job in jobs:
                # TODO: handle these permutations
                job_rows = self.jobs.retrieve(job_id = job['id'])
                job_db = job_rows[0]
                task = self.jobs.taskForJob(job_db, person=person)
                task = self.objects.infoFor(task)
                rootPath = os.path.join(job_db.path, "..")
                runReport = self.jobs.pullRunReport(task, rootPath)

                # We want to lock the input's index to what it was before
                objectIndex = task['runs'].get('index')
                if objectIndex:
                  input_info['index'] = objectIndex

                if isinstance(runReport, list) and len(runReport) > 0:
                  runReport = runReport[0]

                if runReport:
                  print("UPDATING RUN REPORT FOR INPUT")
                  print(runReport)
                  input_info['run'] = input_info.get('run', {})
                  input_info['run'].update(runReport)
                  print(input_info['run'])

                # We then, also, want to add the input's runtime
                # dependencies as normal dependencies.
                input_info['dependencies'] = input_info.get('dependencies', [])
                input_info['dependencies'].extend(input_info.get('run', {}).get('dependencies', []))

            inputs[-1][-1].append([input_info])

    # Permute the input sets and generate partial tasks
    # TODO: permute
    # for now we will take the first item in each input
    runs = self.permuteRun(inputs)
    ret = []

    # TODO: ignore nodes that already executed until loops are fully supported
    import sql
    runJobsTable = sql.Table("run_jobs")
    session = self.database.session()
    query = runJobsTable.select(where = (runJobsTable.run_id == run.id))
    self.database.execute(session, query)

    rows = self.database.many(session, size=1000)
    for row in rows:
      if row.get("node_index") == node.index:
        return [ret]

    for run in runs:
      newNode = node.raw.copy()
      newNode['input'] = run
      if existingTask:
        newNode['task'] = existingTask
        newNode['path'] = taskPath
      ret.append(newNode)

    # If there are no inputs connected, then it still needs to run
    if (nodeCanRun and len(runs)==0):
      newNode = node.raw.copy()
      newNode['input'] = []
      if existingTask:
        newNode['task'] = existingTask
        newNode['path'] = taskPath
      ret.append(newNode)

    return [ret]

  def permuteRun(self, runSpace):
    """
    """

    if len(runSpace) == 0:
      return []

    print("permute runs", runSpace)

    # For each permutation of the remaining inputs,
    # We create a run by adding this input
    runs = self.permuteRun(runSpace[:-1])
    currentCount = len(runs)

    currentWire = runSpace[-1]

    for index, wire in enumerate(currentWire):
      for sourceIndex, item in enumerate(wire):
        print("looking at", item, "for index", index, "from", sourceIndex)
        # If we have no permutations yet, just add the input list as the
        # first run. We will permute this set with other inputs below.
        if currentCount == 0:
          print("initializing run set with item")
          runs.append([item])

        # Add the run to the current list (if there exists one)
        for input in runs[0:currentCount]:
          if sourceIndex == 0:
            print("appending to run set")
            # First, go through each run and add the given item
            # This updates the basic runs, but for the next permutation
            # of this particular input, we go to the bottom else case.
            input.append(item)
          else:
            print("permuting existing run set")
            # Duplicate the existing run input set.
            runs.append(input[:])

            # Recall above that we added the item to the existing set,
            # So now we simply replace that added item with this new item
            runs[-1][-1] = item

    if len(currentWire) == 0:
      if currentCount == 0:
        runs.append([[]])

      for input in runs[0:currentCount]:
        input.append([])

    print("returning", runs)
    return runs

  def tailNodes(self, workflowObject, person = None):
    """ Returns a list of nodes that have a dangling output.
    """

    workflow = self.create_workflow(workflowObject, person=person)
    return workflow.getTailNodes()

  def generateTasks(self, run, person = None):
    """ Returns a set of task objects for the given workflow run.
    """

    ret = []

    for node in run:
      # Look at the node and its inputs/outputs
      # If it is runnable, generate a task

      # Get the object
      uuid     = node['id']
      revision = node['revision']

      obj = self.objects.retrieve(uuid     = uuid,
                                  revision = revision,
                                  person   = person)

      if obj is None:
        return None

      inputs = node.get('input', [])

      existingTask = node.get('task')
      taskPath = node.get('path')
      if existingTask:
        print("REUSING TASK at", taskPath)

      task = self.manifests.run(object       = obj,
                                uuid         = uuid,
                                revision     = revision,
                                inputs       = inputs,
                                person       = person)

      ret.append(task)

    return ret

  def runTask(self, task):
    """
    """

  def lockRun(self, runId):
    """ Locks the run record to give exclusive access to modifying the run metadata.

    Will return None if the run cannot be locked, such as if it does not exist.
    """

    # Get the initial run
    run_db = self.retrieveRun(runId)

    # This run does not exist
    if run_db is None:
      return None

    import sql
    import time

    runs = sql.Table('runs')

    session = self.database.session()

    while True:
      # Attempt to lock the record
      query = runs.update(where   = (runs.id == runId) & (runs.lock == 0),
                          columns = [runs.lock],
                          values  = [1])

      count = self.database.execute(session, query)
      self.database.commit(session)

      # If the record was updated, then we succeeded
      if count == 1:
        return run_db

      # Sleep to reduce the query load and so we don't DoS the database
      time.sleep(1)

    # Could not lock
    return None

  def unlockRun(self, runRecord):
    """ Unlocks the run record so that it may be amended.
    """

    # We don't check at all since we should already be the only owner
    runRecord.lock = 0

    # Just save it and get on with our lives
    session = self.database.session()
    self.database.update(session, runRecord)
    self.database.commit(session)

    # Return the run record
    return runRecord

  def queue(self, object, experiment=None, person = None):
    """ Creates a queued workflow and spawns initial jobs.
    """

    # Discover the workflow
    objInfo = self.objects.infoFor(object)
    #try:
    workflow = self.resolve(object, person=person)
    #except ValueError:
    #  return None
    #except:
    #  WorkflowManager.Log.error("Unknown exception.")
    #  WorkflowManager.Log.error(
    #    "Object is of type %s and does not contain a workflow." %
    #      (objInfo.get('type', 'object'))
    #  )
    #  return None

    job = self.createJob(workflow,
                         person=person)
    run = self.createRun(experiment or object, workflow, job, person)

    self.mapJobToRun(run.id, job.id, None)

    # Return the run record
    return run

  def rootJobFor(self, jobId):
    """ Returns the root job for the given job id.
    """

    run = self.runFor(jobId)

  def runJobFor(self, jobId):
    """ Returns the RunJobRecord for the given job.
    """

    import sql
    runJobs = sql.Table("run_jobs")

    session = self.database.session()

    query = runJobs.select(where = (runJobs.job_id == jobId))

    self.database.execute(session, query)
    row = self.database.fetch(session)

    if not row:
      return None

    from occam.workflows.records.run_job import RunJobRecord

    return RunJobRecord(row)

  def nodesIn(self, workflow, person = None):
    """ Returns the nodes within the given workflow.
    """
    return workflow.nodes

  def nodeAt(self, workflow, nodeIndex, person = None):
    """ Returns information about the node at the given index in the given workflow.
    """
    return self.nodesIn(workflow, person = person)[nodeIndex]

  def completeJob(self, job, person = None):
    """
    """

    print(job._data)

    # Generate tasks for this node with new inputs
    self._updateWorkflow(person=person, finished_job=job, async=False)
    # Queue those tasks

  def finish(self, run):
    """ Completes a run as a success.
    """

    import datetime
    run.finish_time = datetime.datetime.now()

    session = self.database.session()

    self.database.update(session, run)
    self.database.commit(session)

  def fail(self, run):
    """ Completes a run as a failure.
    """

    import datetime
    run.failure_time = datetime.datetime.now()

    session = self.database.session()

    self.database.update(session, run)
    self.database.commit(session)

  def objectForRun(self, runId, person = None):
    """ Returns the containing object that has spawned the workflow run.
    """

    run_db = self.retrieveRun(runId)

    ret = self.objects.retrieve(uuid     = run_db.object_uid,
                                revision = run_db.object_revision,
                                person   = person)

    return ret

  def workflowForRun(self, runId, person = None):
    """ Returns the Object representing the workflow that is reflected in the run.
    """

    run_db = self.retrieveRun(runId)

    ret = self.objects.retrieve(uuid     = run_db.workflow_uid,
                                revision = run_db.workflow_revision,
                                person   = person)

    return ret

  def runsForObject(self, object, revision = None, person=None):
    """ Returns a RunRecord list for the given object.

    When the given revision is None, then this will return all runs. Otherwise,
    it will limit the search to the given revision.
    """

    from occam.workflows.records.run import RunRecord

    import sql

    session = self.database.session()

    runs = sql.Table('runs')

    query = runs.select()
    query.where = (runs.object_uid == object.uuid)

    if revision:
      query.where = (runs.object_revision == revision)

    self.database.execute(session, query)
    rows = self.database.many(session, size=1000)

    return [RunRecord(x) for x in rows]

  def mapJobToRun(self, run_id, job_id, nodeIndex):
    """ Attaches the given job to the given run.
    """

    from occam.workflows.records.run_job import RunJobRecord

    session = self.database.session()

    runJob_db  = RunJobRecord()
    runJob_db.run_id = run_id
    runJob_db.job_id = job_id
    runJob_db.node_index = nodeIndex

    self.database.update(session, runJob_db)
    self.database.commit(session)

    return runJob_db

  def retrieveRun(self, runId, object = None):
    """ Returns the RunRecord for the given run id.
    """

    from occam.workflows.records.run import RunRecord

    import sql

    session = self.database.session()

    runs = sql.Table('runs')

    query = runs.select()
    query.where = (runs.id == int(runId))

    if object is not None:
      query.where = query.where & (runs.object_uid == object.uuid)

    self.database.execute(session, query)
    ret = self.database.fetch(session)

    if ret:
      ret = RunRecord(ret)

    return ret

  def runFor(self, jobId):
    """ Returns the RunRecord corresponding to the given job.
    """

    from occam.workflows.records.run import RunRecord

    import sql

    session = self.database.session()

    runs = sql.Table('runs')
    runJobs = sql.Table('run_jobs')

    subQuery = runJobs.select(runJobs.run_id)
    subQuery.where = (runJobs.job_id == int(jobId))

    query = runs.select()
    query.where = (runs.id.in_(subQuery))

    self.database.execute(session, query)
    ret = self.database.fetch(session)

    if ret:
      ret = RunRecord(ret)

    return ret

  def nodesFor(self, runId):
    """ Yields nodes information about this run.
    """

  def jobsFor(self, runId, status = None, statusNot = None, nodeIndex = None):
    """ Yields the job ids for the given run.
    """

    import sql

    session = self.database.session()

    runs = sql.Table('runs')
    runJobs = sql.Table('run_jobs')
    jobs = sql.Table('jobs')

    query = runJobs.select(runJobs.job_id, runJobs.node_index)
    query.where = (runJobs.run_id == runId)
    if nodeIndex is not None:
      query.where = query.where & (runJobs.node_index == nodeIndex)

    join = query.join(jobs, type_ = "LEFT")
    join.condition = (join.right.id == query.job_id)
    if status is not None:
      if not isinstance(status, list):
        status = [status]
      for statusCheck in status:
        join.condition = join.condition & (join.right.status == statusCheck)
      join.condition = join.condition & (join.right.status == status)
    if statusNot is not None:
      if not isinstance(statusNot, list):
        statusNot = [statusNot]
      for statusCheck in statusNot:
        join.condition = join.condition & (join.right.status != statusCheck)
    query = join.select()

    self.database.execute(session, query)
    rows = self.database.many(session, size=1000)

    ret = {}

    for x in rows:
      if x['node_index'] is None:
        continue
      if x['status'] is None:
        continue
      ret[x['node_index']] = ret.get(x['node_index'], {"jobs": []})
      ret[x['node_index']]['jobs'].append({"id": x['job_id'],
                                           "status": x['status'],
                                           "kind": x['kind'],
                                           "startTime": x['start_time'] and x['start_time'].isoformat(),
                                           "queueTime": x['queue_time'] and x['queue_time'].isoformat()})

    return {"nodes": ret}

  def isInputSourceSelf(self, workflow, node, wireIndex, index):
    pin = node.followInputWire(index)[index]
    return pin[1] < 0

  def resolveInputSourceWire(self, workflow, node, wireIndex, index):
    pin = node.followInputWire(wireIndex)[index]
    if pin[1] < 0:
      # It is the self pin
      return workflow.nodes[pin[0]].self

    return workflow.nodes[pin[0]].outputs[pin[1]]

  def resolveInputSource(self, workflow, node, wireIndex, index):
    print(node.raw)
    print(wireIndex, index)
    pin = node.followInputWire(wireIndex)[index]
    return workflow.nodes[pin[0]]

  def resolveInputs(self, run, workflow, node, pin, person):
    """ This function returns the objects connected to the given input pin for the given node.

    Arguments:
      workflow(Workflow): The running workflow
      node(Node): The node
      pin(int): The pin index
      person(Object): the person running the workflow

    Returns:
      list: List of objects connected to the given pin.
    """

    # If pin is not dangling return whatever is connected to it
    if len(node.inputs[pin].get("connections",[])) != 0 or node.inputs[pin].get("type","")=="configuration":
      inputs = []
      for input in node.followInputWire(pin):
        node_index = input[0]
        node = workflow.nodes[node_index]
        pin_index = input[1]

        # If this is the self pin, well, the node is its own output
        if pin_index == -1:
          outputs = [{
            "id":       node.uuid,
            "revision": node.revision
          }]
        else:
          # Otherwise, we gather the outputs the normal way
          print("DETERMINE OUTPUTS")
          outputs = self._resolveOutputPin(run,
                                           workflow,
                                           workflow.nodes[node_index],
                                           pin_index,
                                           person)
          print(outputs)

        for output in outputs:
          obj = self.objects.retrieve(uuid     = output.get("id"),
                                      revision = output.get("revision"),
                                      person   = person)
          inputs.append(obj)

      return inputs
    # Otherwise, check if this workflow is being executed by another and get
    # the inputs from it.
    else:
      # Is this workflow being executed within another?
      # Get the run it belongs to, if any, then is part of another
      #   workflow run. Recurse into that workflow!
      super_run = None
      if super_run is not None:
        outside_node, outside_pin = self._inputPinTranslation(workflow_run,
                                                              node,
                                                              pin)
        return self.resolveInputs(run, workflow, outside_node, outside_pin, person)
      else:
        return []

  def _resolveOutputPin(self, run, workflow, node, pin, person):
    # If type is not workflow, then just return the output on the node
    if node.type != "workflow":
      return self._getPinOutputs(run,
                                 node,
                                 pin)
    ## Else, if type is workflow, then recurse into the correct subnode ##
    else:
      ## Discover the output node in the subworkflow ##
      # Get the run it belongs to, if any, then is part of another
      #   workflow run. Recurse into that workflow!
      sub_run = workflow_run.sub_run(node.index)
      if sub_run is not None:
        inside_node, inside_pin = self._outputPinTranslation(workflow_run,
                                                            node,
                                                            pin)
        return self._resolveOutputPin(sub_run, inside_node, inside_pin, person)
      else:
        return None

  def _inputPinTranslation(self, inside_run, inside_node, inside_pin):
    ## Get job running the workflow ##
    job = inside_run.job
    ## Get the workflow running the workflow ##
    super_run = inside_run.super_run
    if super_run is None:
      WorkflowManager.Log.error(
        "No super run was found_inputPinTranslation is being called \
          for a non sub-workflow."
      )
      return None

    ## Get the sub-workflow node index ##
    run_job = self.runJobFor(job.id)
    super_node = super_run.workflow.nodes[run_job.node_index]

    lut=[]
    port_count=0
    for node_idx, node in enumerate(inside_run.workflow.nodes):
      lut.append([])
      for input_idx, input in enumerate(node.inputs):
        lut[-1].append(None)
        if input.get("type")!="configuration" and len(input.get('connections', []))==0:
          lut[-1][-1]=port_count
          port_count += 1
    return super_node, lut[inside_node.index][inside_pin]

  def _outputPinTranslation(self, outside_run, outside_node, outside_pin):
    """ this function has a lot of code in common with inputPinTranslation
         they can probably be united """
    ## Get the workflow runned by the node ##
    sub_run = outside_run.sub_run(outside_node.index)
    if sub_run is None:
      WorkflowManager.Log.error(
        "Node %d did not run a workflow."
      )
      return None

    ## Get job running the workflow ##
    # job = sub_run.job

    ## Get the sub-workflow node index ##
    # run_job = self.runJobFor(job.id)
    # sub_node = sub_run.workflow.nodes[run_job.node_index]

    lut=[]
    for node_idx, node in enumerate(sub_run.workflow.nodes):
      for output_idx, output in enumerate(node.outputs):
        if len(output.get('connections', []))==0:
          lut.append(None)
          lut[-1]=[node_idx, output_idx]
    return sub_run.workflow.nodes[lut[outside_pin][0]], lut[outside_pin][1]

  ##
  #
  #  This function takes an OCCAM object and returns it as a Workflow class
  #    or the first workflow contained in the object.
  #
  #  @param self The object pointer.
  #  @param object The OCCAM object
  #  @param person The person running the workflow
  #
  def resolve(self, object, person=None):
    try:
      info = self.objects.infoFor(object)
      data = {}
      if 'file' in info:
        try:
          data = self.objects.retrieveJSONFrom(object, info.get('file'))
        except:
          pass
      workflow = Workflow(info, data, object.revision)
    except ValueError:
      try:
        workflow_object = self._getTypesWithin(object, "workflow", person)
        workflow_object = workflow_object[0]
        info = self.objects.infoFor(object)
        data = {}
        if 'file' in info:
          try:
            data = self.objects.retrieveJSONFrom(object, info.get('file'))
          except:
            pass
        workflow = Workflow(info, data, object.revision)
      except ValueError as e:
        WorkflowManager.Log.error(
          "Object is of type %s and does not contain a workflow." %
            (objInfo.get('type', 'object'))
        )
        raise e
    return workflow

  def _updateWorkflow(self, finished_job, person=None, async=True):
    """ This function generates newly available jobs when a job is completed.

    Arguments:
      finished_job(JobRecord): The job that triggers the update
      person(Object): The person running the workflow
      async(boolean): If true a daemon is launched to run the generated jobs
    """

    # Retrieve the run that owns this job
    # First retrieve the run-job connection
    runJob = self.runJobFor(finished_job.id)
    if runJob is None:
      WorkflowManager.Log.error(
        "Job with the ID %d is not running a workflow." %
          (finished_job.id)
      )
      raise ValueError

    # Then retrieve the run
    run = self.retrieveRun(runJob.run_id)
    if run is None:
      WorkflowManager.Log.error(
        "Could not find run with ID %d." %
          (runJob.run_id)
      )
      raise ValueError

    # Get the workflow object that owns this job
    object = self.objects.retrieve(uuid     = run.workflow_uid,
                                   revision = run.workflow_revision,
                                   person   = person)
    if object is None:
      WorkflowManager.Log.error(
        "Could not find object %s@%s." %
          (run.workflow_uid, run.workflow_revision)
      )
      raise ValueError

    objInfo = self.objects.infoFor(object)
    try:
      workflow = self.resolve(object, person=person)
    except ValueError:
      return None
    except:
      WorkflowManager.Log.error("Unknown exception.")
      WorkflowManager.Log.error(
        "Object is of type %s and does not contain a workflow." %
          (objInfo.get('type', 'object'))
      )
      raise ValueError

    # Retrieve the node that was executed by this job
    finished_node = workflow.nodes[runJob.node_index]
    candidate_nodes = []
    visited_nodes = []

    # TODO: look at whether or not this was running the 'script'
    #       and update that node's run section accordingly

    # Crawl the self wire
    # This happens when a scripted node is attached to a running consumer
    output_nodes = finished_node.followSelf()
    task = None
    taskPath = None
    if output_nodes:
      print("AHHHH. GOING DOWN THAT PATH")

      # Get the job that ran this and determine its report
      jobs = self.jobsFor(run.id, nodeIndex = finished_node.index).get('nodes', {}).get(finished_node.index, {}).get('jobs', [])
      for job in jobs:
        # TODO: handle these permutations
        job_rows = self.jobs.retrieve(job_id = job['id'])
        job_db = job_rows[0]
        task = self.jobs.taskForJob(job_db, person=person)
        task = self.objects.infoFor(task)
        taskPath = job_db.path

    for output_node_to in output_nodes:
      node_idx = output_node_to[0]
      if node_idx not in visited_nodes:
        visited_nodes.append(node_idx)
        node = workflow.nodes[node_idx]
        candidate_nodes.append({
          "node": node,
          "task": task,
          "path": taskPath
        })

    # Get output nodes as candidates for new executions
    for idx, output in enumerate(finished_node.outputs):
      output_nodes = finished_node.followOutputWire(idx)
      for output_node_to in output_nodes:
        node_idx = output_node_to[0]
        if node_idx not in visited_nodes:
          visited_nodes.append(node_idx)
          node = workflow.nodes[node_idx]
          candidate_nodes.append({
            "node": node
          })

    # Try to run the candidate nodes
    self._runNodes(workflow, candidate_nodes, run, person=person, async=async)

  def launch(self, object, run_db, person=None, inputs=None, async=True):
    """ This function takes a workflow and runs it.

    Arguments:
      object(Object): The OCCAM workflow object
      person(Object): The person running the workflow
      inputs(list): A list of inputs to override the workflow connections
      async(boolean): If true a daemon is launched to run the generated jobs
    """

    # Store the object info: it can be an experiment
    # that contains a workflow
    objInfo = self.objects.infoFor(object)

    # Get the actual workflow (either the object itself
    # or the first workflow in it)

    #try:
    workflow = self.resolve(object, person=person)
    #except ValueError:
    #  return None
    #except:
    #  WorkflowManager.Log.error("Unknown exception.")
    #  WorkflowManager.Log.error(
    #    "Object is of type %s and does not contain a workflow." %
    #      (objInfo.get('type', 'object'))
    #  )

    # Get a list of nodes that can execute, i.e. nodes
    # that do not depend on other results
    nodes = self._getInitialNodes(workflow, person)

    # Run those nodes
    self._runNodes(workflow, nodes, run_db, person=person, async=async)

  def createJob(self, workflow, run_owner=None, run=None, existingPath = None, person=None):
    """ This function creates a job for the input object.

    Arguments:
      object(Object): The object that will be run by the job
      run_owner(RunRecord): The run that is executing the job
      run(RunRecord): The run that the job is executing (if workflow, not task)
      person(Object): The person running the workflow

    Returns:
      JobRecord: the created job
    """

    objectInfo = workflow.info

    # Top level workflows have no run_owner and aren't finalized
    finalize = None
    finalizeTag = None
    if run_owner is not None:
      finalize = "workflows"
      finalizeTag = str(run_owner.id)

    job = self.jobs.create(objectInfo,
                           revision     = workflow.revision,
                           person       = person,
                           finalize     = finalize,
                           finalizeTag  = finalizeTag,
                           existingPath = existingPath)
    return job

  def _runExecutedByJob(self, job_id):
    """ Get the run that a job is executing.

    Arguments:
      job_id(int): The id of the job

    Returns:
      RunRecord: The run the job is executing, or None.
    """

    import sql

    # Get the run that is executed by job_id
    # Get connection job->run to identify which run is executed by job_id
    runJob = self.runJobFor(jobId=job_id)
    if runJob is None:
      WorkflowManager.Log.warning(
        "Job %s is not running a workflow." %
          (job_id)
      )
      return None

    # Get the run that job_id executes
    run = self.retrieveRun(runJob.run_id)
    if run is None:
      WorkflowManager.Log.error(
        "Could not find run %s." %
          (runJob.run_id)
      )
      return None

    return run

  def _runNodes(self, workflow, nodes, run_db, person=None, async=True):
    """ This function creates the jobs for a list of nodes, if they can run

    Arguments:
      workflow(Workflow): The workflow that is running the nodes (prob. redundant)
      nodes(list): A list of nodes to be executed
      run_db(RunRecord): The run that is running the nodes
      person(Object): The person running the nodes
      async(boolean): If true a daemon is launched to run the jobs
    """

    runs = []

    # Useful for debugging
    if len(nodes) == 0:
      WorkflowManager.Log.warning("The list of nodes to run is empty.")

    # Create the information for the nodes run
    for info in nodes:
      # TODO: Check how this function handles inputs!
      #       A run can contain multiple nodes!!! How, and why? Permutations?
      new_run = self.generateRunForNode(workflow, info, run_db, person = person)
      runs.extend(new_run)

    # Split workflow runs from task runs
    runs_no_workflows = []
    runs_workflows = []
    for run in runs:
      run_no_workflow = []
      run_workflow = []
      for node in run:
        if node.get("type") != "workflow":
          run_no_workflow.append(node)
        else:
          run_workflow.append(node)
      runs_no_workflows.append(run_no_workflow)
      runs_workflows.append(run_workflow)

    for run in runs_workflows:
      # Get sub workflow
      for node in run:
        sub_workflow_obj = self.objects.retrieve(uuid = node.get('id'),
                                                 revision = node.get('revision'),
                                                 person   = person)
        sub_workflow = self.resolve(sub_workflow_obj, person=person)

        ## Set owner to top level workflow owner
        #    (the one that started the run)
        owner = self.objects.retrieve(uuid     = run_db.object_uid,
                                      revision = run_db.object_revision,
                                      person   = person)
        ## Create the run and job ##
        new_wf_run = self.createRun(owner, sub_workflow, person)
        job = self.createJob(sub_workflow,
                             run_owner=run_db,
                             run=new_wf_run,
                             person=person)
        self.mapJobToRun(run_db.id, job.id, node['index'])

        WorkflowManager.Log.write("Created job %s." % (job.id))

    # Generate tasks
    # For every run (set of nodes to execute together)
    for run in runs_no_workflows:
      print("generating task for run")
      print(run)

      # Generate each running task
      tasks = self.generateTasks(run, person=person)

      for node, task in zip(run, tasks):
        taskInfo = self.objects.infoFor(task)
        job = self.createJob(task,
                             run_owner=run_db,
                             existingPath=node.get('path'),
                             person=person)
        self.mapJobToRun(run_db.id, job.id, node['index'])
        WorkflowManager.Log.write("Created job %s." % (job.id))

    if async:
      self.jobs.launch_daemon()

  def _getPinOutputs(self, run, node, pin_index):
    """ This function returns a list of the ids and revisions of the objects generated by a node in a certain pin.

    Arguments:
      run The run that executed the node
      node A The node whose output is being probed
      pin_index The index of the pin being probed

    Returns:
      list: Ids/revisions of objects.
    """

    pin_outputs = []

    ## Make sure the pin index is valid ##
    if pin_index >= len(node.outputs) or pin_index < 0:
      WorkflowManager.Log.error(
        "Trying to access pin %d out of %d." %
        (pin_index, len(node.outputs))
      )
      raise IndexError

    import sql

    ## Query the database for the output objects of the run
    # Get tables with the run outputs and the corresponding objects
    run_outputs = sql.Table("run_outputs")
    run_output_objects = sql.Table("run_output_objects")
    session = self.database.session()

    # Join the tables by run_id and job_id
    join = run_outputs.join(run_output_objects)
    join.condition = (
                       (run_output_objects.run_id == run_outputs.run_id) &
                       (run_output_objects.job_id == run_outputs.job_id) &
                       (run_output_objects.output_index == run_outputs.output_index)
                     )
    # Query the join for the output objects id and revisions for the pin #
    query_for_outputs = join.select(
      run_output_objects.object_uid,
      run_output_objects.object_revision,
        where =
          ( run_outputs.node_index == str(node.index) ) &
          ( run_outputs.output_index == str(pin_index) ) &
          ( run_outputs.run_id == str(run.id) )
    )
    self.database.execute(session, query_for_outputs)
    run_output_results = self.database.many(session, size=1000)

    ## Structure return ##
    for object in run_output_results:
      pin_outputs.append({
        "id" :       str(object.get("object_uid")),
        "revision" : str(object.get("object_revision"))
      })

    return pin_outputs

  ##
  #
  #  This function returns a list of the ids and revisions of the objects
  #    generated by all pins of a node
  #
  #  @param self The object pointer.
  #  @param run The run that executed the node
  #  @param node A The node whose output is being probed
  #
  def _getNodeOutputs(self, run, node):

    ret = []
    ## For each pin call specialized function ##
    for output_idx, output in enumerate(node.outputs):
      pin_outputs = self._getPinOutputs(run, node, output_idx)
      ret.append(pin_outputs)
    return ret

  ##
  #
  #  This function returns a list of the nodes that do not depend
  #    on other results
  #
  #  @param self The object pointer.
  #  @param workflow The workflow to inspect.
  #  @param person The person performing the operation
  #
  def _getInitialNodes(self, workflow, person=None):
    nodes = []
    visited_nodes=[]

    # Get all head nodes (nodes without connected inputs)
    headNodes = workflow.getHeadNodes()
    for nodeIndex, node in enumerate(headNodes):
      # if one of those is a non executable input, follow the wire
      if not (self.isNodeRunnable(node, person=person) or
              self.isNodeWorkflow(node, person=person)):
        outputs = []
        outputs.append(node.self)
        outputs.extend(node.outputs)
        for wireIndex, wire in enumerate(outputs):
          for output in wire.get('connections', []):
            subNodeIndex, inputIndex, position = output.get('to')

            # Ignore visited nodes
            if subNodeIndex in visited_nodes:
              continue

            # Keep a list of visited nodes to avoid inserting the same node
            visited_nodes.append(subNodeIndex)
            visited_nodes=list(set(visited_nodes))
            subNode = workflow.nodes[subNodeIndex]
            nodes.append({
              "node": subNode
            })
      else:
        if nodeIndex in visited_nodes:
          continue

        # Keep a list of visited nodes to avoid inserting the same node
        visited_nodes.append(nodeIndex)
        visited_nodes=list(set(visited_nodes))
        nodes.append({
          "node": node
        })
    return nodes

  def _getRunnableNodes(self, workflow, run, person=None):
    # TODO: Extend to cyclic graphs
    nodes = []
    import sql
    from occam.workflows.records.run_job import RunJobRecord

    # Get already executed nodes
    runJobsTable = sql.Table("run_jobs")
    session = self.database.session()
    query = runJobsTable.select(where = (runJobsTable.run_id == run.id))
    self.database.execute(session, query)
    rows = self.database.many(session, size=1000)

    # Nothing was executed yet
    if len(rows) == 0:
      nodes = self._getInitialNodes(workflow,person)
    else:
      #TODO: Get next nodes
      for row in rows:
        node_idx = row["node_index"]
        executed_node = workflow.nodes[node_idx]
        self._getNodeOutputs(run, executed_node)
        for output_idx, output in enumerate(workflow.nodes[node_idx].outputs):
          # get indices (node, port)
          next_nodes_to = executed_node.followOutputWire(output_idx)
          next_nodes=[]
          # convert indexes to nodes ignoring dulpicates
          nodes_visited = []
          for next_node_to in next_nodes_to:
            if next_node_to[0] not in nodes_visited:
              nodes_visited.append(next_node_to[0])
              next_nodes.append(workflow.nodes[next_node_to[0]])


          for next_node in next_nodes:
            # TODO: Check all possible runs for this node (sets of inputs)
            #   This requires going to the database, and ask for all executable nodes, and inputs.
            #   Then, go through the job DB and check if that has already run...

            can_execute=True
            inputs=[]
            for input_idx, input in enumerate(next_node.inputs):
              # Assuming only one
              node_in_input_to = next_node.followInputWire(output_idx)[0]
              input_node = workflow.nodes[node_in_input_to[0]]
              # Check if input is connected to self's
              # Append those to list
              inputs.append(self._getNodeOutputs(run, input_node)[node_in_input_to[1]])
              # For each permutation of inputs, chck if there is already a job with such a task
            if can_execute:
              nodes.append(next_node)
      pass
    return nodes

  def createRun(self, owner, workflow, job, person=None):
    from occam.workflows.records.run import RunRecord
    session = self.database.session()

    run_db = RunRecord()

    # Unlock at first
    run_db.lock = 0

    # Establish the origin workflow container
    ownerInfo = self.objects.infoFor(owner)
    run_db.object_name     = ownerInfo.get('name')
    run_db.object_uid      = ownerInfo.get('id')
    run_db.object_revision = owner.revision

    run_db.workflow_name     = workflow.name
    run_db.workflow_uid      = workflow.uuid
    run_db.workflow_revision = workflow.revision

    # Set up root job
    run_db.job_id = job.id

    # Establish when this is queued
    import datetime
    run_db.queue_time = datetime.datetime.now()

    # Establish who asked for this
    if person:
      run_db.person_uid = person.uuid

    # Add to the database
    self.database.update(session, run_db)
    self.database.commit(session)

    return run_db

  # TODO: Maybe add this to class object
  def _getTypesWithin(self, object, type, person=None):
    ret = []
    # Determine the actual workflow
    objInfo = self.objects.infoFor(object)

    # Look for a workflow within
    for contained in objInfo.get('contains', []):
      if contained.get('type') == type:
        ret.append(  self.objects.retrieve(uuid     = contained.get('id'),
                                           revision = contained.get('revision'),
                                           person   = person)
        )
    return ret
