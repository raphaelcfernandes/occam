# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json

from occam.commands.manager import command, option, argument

from occam.object import Object
from occam.log    import Log

from occam.manager import uses

from occam.objects.write_manager  import ObjectWriteManager
from occam.configurations.manager import ConfigurationManager
from occam.workflows.manager      import WorkflowManager
from occam.jobs.manager           import JobManager
from occam.databases.manager import DatabaseManager

@command('workflows', 'run',
  category      = 'Workflow Management',
  documentation = "Appends an object to a workflow")
@argument("workflow",   type = "object",
                        help = "The workflow or a containing object")
@option("-j", "--json", dest   = "to_json",
                        action = "store_true",
                        help   = "returns result as a json document")
@uses(ObjectWriteManager)
@uses(ConfigurationManager)
@uses(WorkflowManager)
@uses(JobManager)
@uses(DatabaseManager)
class RunCommand:
  def do(self):
    # Get the workflow to run
    workflow = self.objects.resolve(self.options.workflow, person = self.person)

    if workflow is None:
      Log.error("cannot find workflow with id %s" % (self.options.workflow.id))
      return -1

    # self.workflows.queue(workflow, person=self.person)
    # return;

    self.run(workflow)
    return 0

  def run(self, workflow):
    # Get the workflow to run
    workflow = self.objects.resolve(self.options.workflow, person = self.person)

    if workflow is None:
      Log.error("cannot find workflow with id %s" % (self.options.workflow.id))
      return -1

    run = self.workflows.queue(workflow, person = self.person)

    Log.output(json.dumps({
      "job": {
        "id": self.jobs._jobThatRunsThisWorkflow(run.id)[0].id
      },
      "run": {
        "id": run.id,
        "queueTime": run.queue_time and run.queue_time.isoformat()
      }
    }))

    Log.done("Successfully queued this workflow.")

    return 0
