# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.object import Object
from occam.log    import loggable

from occam.manager import manager, uses

from occam.storage.manager       import StorageManager
from occam.nodes.manager         import NodeManager
from occam.network.manager       import NetworkManager
from occam.objects.write_manager import ObjectWriteManager

@loggable
@manager("discover")
@uses(StorageManager)
@uses(NodeManager)
@uses(NetworkManager)
@uses(ObjectWriteManager)
class DiscoverManager:
  """ This manages mechanisms for finding objects in the world at large. This
      gives the discovery capability to any module that needs it.

      Discovery is sometimes passed into routines that might require it. For
      instance, routines that might lookup subobjects that are linked might
      want a discovery agent to use to do that lookup. You can provide this
      manager as that agent.

      It is possible that down the line we will allow alternative agents or
      extensions that will enable different modes of discovery through
      services or various archive or publication venues that are outside of
      the federation.
  """

  def resolve(self, option, person = None):
    """ Returns an Object based on a CLI object argument.

    If it is given a URL, it attempts to discover the object through the normal
    means. The scheme of the url will depict the storage layer to query.

    If it is not a URL, but an Occam uuid and revision, it will search for the
    object within the federation and discover it through the normal channels.
    """

    if option is None:
      return None

    if self.network.isURL(option.id):
      parts = self.network.parseURL(option.id)
      objInfo = self.storage.retrieve(parts.scheme, parts.netloc)

      ret = None
    else:
      # TODO: check the normal local repository and then react only if the
      #       object is not found.
      ret = self.objects.resolve(option, person = person)
      if ret is None:
        ret = self.discover(option.id, revision = option.revision)

    return ret

  def discover(self, uuid, revision=None):
    """
    Uses the various storage backends to hopefully find and retrieve the object
    information. Returns the Object that has been discovered.
    """

    DiscoverManager.Log.noisy("Attempting to discover object %s@%s" % (uuid, revision))

    hosts = self.storage.discover(uuid, revision=revision)

    if len(hosts) > 0:
      # Start asking the hosts for object invocations
      for host in hosts:
        hostURL = "https://%s:9292" % (host)

        # TODO: Optionally, we can also start discovering the nodes via:
        #   self.nodes.discover(hostURL)  # ... for host in hosts

        # TODO: We can maybe select from the list any nodes we already know
        #   or trust

        node = self.nodes.discover(hostURL, untrusted=True, quiet=True)

        if not node is None:
          # Create empty placeholder for Object
          path = self.objects.write.createPathFor(uuid)
          DiscoverManager.Log.write("creating object")

          # Add entry to database (if needed)
          objInfo = self.nodes.pullObjectInfoFrom(node, uuid, revision=revision)
          revision = objInfo.get('revision', revision)

          # Pull Invocation Metadata
          obj = Object(path=None, uuid=uuid, revision=revision, info=objInfo)
          self.nodes.pullInvocationDataFrom(node, obj, ['builds', 'output', 'generated', 'stores/ipfs', 'partialTasks', 'tasks', 'versions'])

          # Store object
          self.objects.write.store(obj)

          return Object(path = obj.path, uuid=uuid, revision=revision)

