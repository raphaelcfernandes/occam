# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sql

from occam.databases.manager import uses, datastore

@datastore("resources")
class ResourceDatabase:
  """ Manages the database interactions for tracking resource objects.
  """

  def retrieveResourceObject(self, resourceType=None, uuid=None, source=None):
    """ Retrieves the database record for the Object that describes the resource.

    The record will be found using the given resourceType, uuid, and source, if
    given.

    If the given resource is not known, then None is returned.
    """

    session = self.database.session()

    objects = sql.Table('objects')

    query = objects.select()
    if not resourceType is None:
      query.where = (objects.resource == resourceType)
    if not uuid is None:
      if query.where:
        query.where = query.where & (objects.uid == uuid)
      else:
        query.where = (objects.uid == uuid)
    if not source is None:
      if query.where:
        query.where = query.where & (objects.source == source)
      else:
        query.where = (objects.source == source)

    from occam.objects.records.object import ObjectRecord

    self.database.execute(session, query)
    return ObjectRecord(self.database.fetch(session))

  def retrieveResource(self, resourceType=None, uuid=None, revision=None, source=None):
    """ Retrieves the database record that describes the instantiation of the given
    resource given its resourceType, uuid, and revision.

    If the resource is not known specifically for this revision, None is
    returned.
    """

    session = self.database.session()

    objects   = sql.Table('objects')
    resources = sql.Table('resources')

    subquery = objects.select(objects.id)
    if not resourceType is None:
      subquery.where = (objects.resource == resourceType)
    if not uuid is None:
      if subquery.where:
        subquery.where = subquery.where & (objects.uid == uuid)
      else:
        subquery.where = (objects.uid == uuid)
    if not source is None:
      if subquery.where:
        subquery.where = subquery.where & (objects.source == source)
      else:
        subquery.where = (objects.source == source)

    query = resources.select()
    query.where = (resources.revision == revision) & (resources.occam_object_id.in_(subquery))

    self.database.execute(session, query)

    from occam.resources.records.resource import ResourceRecord
    return ResourceRecord(self.database.fetch(session))
