# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import json

from occam.config import Config
from occam.log    import loggable
from occam.object import Object

from occam.manager import manager, uses

from occam.storage.manager   import StorageManager

@loggable
@uses(StorageManager)
@manager("resources")
class ResourceManager:
  """ This OCCAM manager handles resource installation.
  
  This is the process OCCAM uses to resolve "install" sections of any
  Object.

  When a resource is newly pulled from source, the uuid and revision/hash
  are not known. So, it is pulled to a temporary place near where the
  resource would be stored and then moved when the name is known. An Object
  and Resource record is created as necessary.

  For files, the resource data is a binary blob. Other types of resources
  are also possible. Git resources are directories that are maintained by
  the git data structure. In these cases, the resource is versioned by its
  own mechanism. Files and binary blobs, on the other hand, are versioned
  through an external mechanism: a hash.

  Therefore, each repository plugin can decide how it handles versioning.
  The FileResource plugin, for example, will be given a path to its
  resource data as stored by the StorageManager and will return a path
  to the requested revision as ``<base path>/<revision hash>``. Whereas a
  GitResource will simple parrot the ``<base path>`` as revisions are kept
  as metadata within the git resource rooted at that path.

  This is why, as strange as it may look when viewing one over the others,
  the resource plugin asks for a path and then repeats that path back to
  the other subroutines within the resource implementation. A FileResource,
  when :meth:`~occam.resources.plugins.file.FileResource.retrieve` is called, 
  passing it ``<base path>/<resource hash>`` allows it to form a direct
  path to the resource at ``<base path>/<resource hash>/data`` whereas the
  GitResource gets passed ``<base path>`` and remotely executes git to pull
  out the file data with ``git show`` using the ``<base path>`` as the
  working directory.

  Examples of different types of Resources are available in plugins. The
  FileResource is an example of a binary blob. The TarResource inherits the
  functionality of a File but adds archival and directory traversal within the
  resource. GitResource shows you a versioned resource that is self-maintained.
  DockerResource gives an example of a resource that is self-stored using some
  external tooling and is not installed as part of an Object's local filesystem.
  """

  handlers = {}

  def __init__(self):
    """
    Initialize the resource manager.
    """

    import occam.resources.plugins.file
    import occam.resources.plugins.tar
    import occam.resources.plugins.git
    import occam.resources.plugins.mercurial
    import occam.resources.plugins.zip
    import occam.resources.plugins.docker

    #self.handlers = ResourceManager.handlers

  @staticmethod
  def register(resourceType, handlerClass):
    """
    Adds a new resource type.
    """
    ResourceManager.handlers[resourceType] = handlerClass

  def handlerFor(self, resourceType):
    """
    Returns an instance of a handler for the given type.
    """

    if not resourceType in self.handlers:
      # TODO: warning?
      resourceType = 'file'

    return self.handlers[resourceType]()

  def retrievePath(self, uuid, path, revision):
    """ Returns the path the object of the given uuid would be stored.
    """

    obj = self.datastore.retrieveResourceObject(uuid=uuid)

    resourceType = None

    if not obj is None:
      resourceType = obj.resource

    if resourceType is None:
      return None

    handler = self.handlerFor(resourceType)
    return handler.pathFor(uuid, path, revision)

  def retrieve(self, uuid, path, revision):
    """ Retrieves the resource given by the uuid and revision and path.
    """

    obj = self.datastore.retrieveResourceObject(uuid=uuid)

    resourceType = None

    if not obj is None:
      resourceType = obj.resource

    if resourceType is None:
      return None

    handler = self.handlerFor(resourceType)
    return handler.pathFor(uuid, path, revision)

  def install(self, resourceInfo, path):
    """ Installs the given resource to the given path.
    """

    if resourceInfo is None:
      return

    # Get resource Type
    resourceType = resourceInfo.get('type', 'file')
    handler = self.handlerFor(resourceType)

    basePath = path

    # Default destination:
    resourceInfo['to'] = resourceInfo.get('to', 'package')

    # Do not allow a 'to' path with a relative ..
    if ".." in resourceInfo['to']:
      ResourceManager.Log.error("Resource's destination path contains a '..' which is not allowed.")
      return False

    path = os.path.join(basePath, resourceInfo['to'])

    uuid = resourceInfo.get('id')
    revision = resourceInfo.get('revision')

    resourcePath = None
    if not revision is None and not uuid is None:
      storagePath = self.storage.resourcePathFor(uuid, revision)
      if handler.exists(uuid, storagePath, revision):
        resourcePath = handler.pathFor(uuid, storagePath, revision)
        ResourceManager.Log.noisy("Installing resource from %s" % (resourcePath))

    if not resourcePath is None:
      ret = handler.install(uuid, revision, resourcePath, resourceInfo, destination=os.path.realpath(path))

      if ret:
        handler.actions(uuid, revision, resourcePath, resourceInfo, destination=os.path.realpath(basePath))

      return ret

    ResourceManager.Log.error("Failed to find resource")

    return False

  def installAll(self, objectInfo, path):
    """ Returns an array of Objects for pulled objects.
    
    This array, like pullAll, correspond directly to the list of
    resources to install in the given Object's 'install' section. This method
    will install the resources to the given path.
    """

    resources = objectInfo.get('install', [])

    for resource in resources:
      new_obj = self.install(resource, path)

      if 'install' in resource:
        self.installAll(resource, path)

    return resources

  def cloneAll(self, obj):
    """ Returns an array of new resource infos for pulled objects.
    
    This array corresponds
    directly to the list of resources to install in the given Object's 'install'
    section. It will clone the resource when possible and update that section to
    point to the cloned version. Otherwise, it will leave the resource info
    alone.
    """

    ret = []

    objectInfo = obj.objectInfo()

    resources = objectInfo.get('install', [])

    if not isinstance(resources, list):
      resources = [resources]

    for resourceInfo in resources:
      new_info = self.clone(resourceInfo)
      ret.append(new_info)

    return ret

  def clone(self, resourceInfo):
    """
    Will clone the given resource, if necessary. If not necessary, will just
    return the resource as is. Generally, this is useful for cloning objects
    that have a git repository attached. When you clone that object, you mean
    to modify the code, so you must also fork the repositories.
    """

    install_type = resourceInfo.get('type', 'object')
    uuid         = resourceInfo.get('id')
    revision     = resourceInfo.get('revision')
    source       = resourceInfo.get('source')
    name         = resourceInfo.get('name')
    to           = resourceInfo.get('to', 'package')

    handler = self.handlerFor(install_type)
    if handler.clonable():
      newResourceInfo = handler.clone(uuid, revision, name, source, to)
      newInfo = resourceInfo.copy()

      # Overwrite identifying tags in the new resource info structure
      for tag in ['id', 'revision']:
        newInfo[tag] = newResourceInfo.get(tag, newInfo.get(tag))

      return newInfo
    else:
      return resourceInfo

  def retrieveFile(self, uuid, revision, path, start=0, length=None):
    """ Pulls out a stream for retrieving the resource data for this object.
    """

    db_obj = self.datastore.retrieveResourceObject(uuid=uuid)

    resourceType = None

    if not db_obj is None:
      resourceType = db_obj.resource

    if resourceType is None:
      return None

    handler = self.handlerFor(resourceType)
    return handler.retrieve(uuid, revision, path)

  def retrieveFileStat(self, uuid, revision, path):
    """ Pulls out the file status for the resource object.
    """

    db_obj = self.datastore.retrieveResourceObject(uuid=uuid)

    resourceType = None

    if not db_obj is None:
      resourceType = db_obj.resource

    if resourceType is None:
      return None

    handler = self.handlerFor(resourceType)
    ret = handler.stat(uuid, revision, path)

    ret["from"] = {
      "type": "resource",
      "subtype": resourceType,
      "id": uuid,
      "revision": revision,
      "path": path
    }

    return ret

  def retrieveDirectoryFrom(self, uuid, revision, path, subpath):
    """ Pulls out a stream for retrieving the given file for this object.
    """

    db_obj = self.datastore.retrieveResourceObject(uuid=uuid)

    resourceType = None

    if not db_obj is None:
      resourceType = db_obj.resource

    if resourceType is None:
      return None

    handler = self.handlerFor(resourceType)
    data = handler.retrieveDirectory(uuid, revision, path, subpath) or {}

    data['items'] = data.get('items', [])

    if not isinstance(data.get('items'), list):
      data['items'] = []

    data['from'] = {
      "type": "resource",
      "subtype": resourceType,
      "id": uuid,
      "revision": revision,
      "path": subpath,
    }

    return data

  def retrieveResource(self, resourceType, uuid, revision, source):
    return self.datastore.retrieveResource(resourceType, uuid, revision, source)

  def retrieveFileStatFrom(self, uuid, revision, path, subpath):
    """ Pulls out the file status for the given path for this object.
    """

    db_obj = self.datastore.retrieveResourceObject(uuid=uuid)

    resourceType = None

    if not db_obj is None:
      resourceType = db_obj.resource

    if resourceType is None:
      return None

    handler = self.handlerFor(resourceType)
    return handler.retrieveFileStat(uuid, revision, path, subpath)

  def retrieveFileFrom(self, uuid, revision, path, subpath, start=0, length=None):
    """ Pulls out a stream for retrieving the given file for this object.
    """

    db_obj = self.datastore.retrieveResourceObject(uuid=uuid)

    resourceType = None

    if not db_obj is None:
      resourceType = db_obj.resource

    if resourceType is None:
      return None

    handler = self.handlerFor(resourceType)
    return handler.retrieveFile(uuid, revision, path, subpath, start=start, length=length)

def resource(name):
  """
  This decorator will register the given class as a resource.
  """

  def register_resource(cls):
    ResourceManager.register(name, cls)
    return cls

  return register_resource
