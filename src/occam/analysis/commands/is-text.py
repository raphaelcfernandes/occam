# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.objects.manager  import ObjectManager
from occam.analysis.manager import AnalysisManager

from occam.log import Log

import json

@command('analysis', 'is-text',
  category      = 'Data Analysis',
  documentation = "Determines if the given object or file within an object can likely be rendered as text.")
@argument("object", type = "object")
@option("-a", "--all",  dest   = "list_all",
                        action = "store_true",
                        help   = "includes all files within resources as well and will redirect to linked resources")
@uses(ObjectManager)
@uses(AnalysisManager)
class IsTextCommand:
  """ Returns an analysis report about whether or not the given object or file is text.

  It may give some information about the kind of text.
  """

  def do(self):
    obj = self.objects.resolve(self.options.object, person = self.person)

    if obj is None:
      Log.error("cannot find the given object.")
      return -1

    if self.options.object.path:
      path = self.options.object.path
    else:
      path = "/object.json"

    data = self.objects.retrieveFileFrom(obj, path, includeResources=self.options.list_all, person=self.person, start = 0, length = 1000)

    report = self.analysis.examineText(data)

    ret = {"result": report["isText"], "report": report}

    Log.output(json.dumps(ret))

    return 0
