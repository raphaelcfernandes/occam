from occam.commands.manager import command, option, argument

from occam.object import Object
from occam.log    import Log

from occam.manager import uses

from occam.daemon.manager  import DaemonManager

@command('daemon', 'run',
  category      = 'Services',
  documentation = "Starts a daemon in the foreground.")
@option("-p", "--port", action = "store",
                        dest   = "port",
                        help   = "determines the port to start the daemon")
@uses(DaemonManager)
class DaemonCommand:
  """ This command starts a daemon on the given port.
  """

  def do(self):
    """ Perform the command.
    """

    self.daemon.run(port = self.options.port)
    return 0
