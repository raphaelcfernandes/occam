# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# The local storage is responsibile for storing all occam data. It is assumed
# to always exist, and data is never deleted from here without an explicit
# command.

# Data is stored in the configurable path, usually in <occam-home>/objects
# Within this directory, the objects are referenced by their UUID. Pieces of
# UUID are keyed such that directory lookups do not crash on some filesystems.
# That is, the uuid 70c44134-8614-11e6-b3c6-f23c910a26c8 will be located within
# /.occam/objects/70/c4/70c44134-8614-11e6-b3c6-f23c910a26c8 and within that
# directory will be subdirectories.

# the /repository directory will have a git repository that will maintain any
# changes to the main object's contents.

# the /resource directory will have a set of hashed data files that pertain to
# the object. These are generally for objects that are wrapping externally
# sourced resources (found within the 'install' part of an object description.
# The hashes are defined by the resource handler associated with the resource.
# File resources, for instance, are simply the digest of the file. GitRepository
# resources do not have a hash to identify them. They merely exist within the
# resource directory. (And distinct from the /repository git package which only
# maintains the object description and metadata history *describing* the git
# resource)

import codecs
import json
import os
import shutil
from uuid import uuid4

from occam.log    import loggable
from occam.config import Config
from occam.git_repository import GitRepository

from occam.storage.manager import storage

@storage('local', priority=0)
class Local:
  """ Implements the logic to make use of IPFS distributed/federated storage.
  """

  def internalPathFor(self, uuid, revision=None, buildId=None, build=False, resource=False):
    """ An internal method to give us the path to a particular thing within the object.
    """

    path = self.storePathFor(uuid)

    if not revision is None:
      if build:
        path = os.path.join(path, "builds", revision)

        if buildId:
          path = os.path.join(path, buildId)
      else:
        path = os.path.join(path, "cache", revision)
    else:
      if buildId:
        path = os.path.join(path, "builds", revision)
        path = os.path.join(path, buildId)
      elif resource:
        path = os.path.join(path, "resource")
      else:
        path = os.path.join(path, "repository")

    return path

  def createPathFor(self, uuid, resource=False):
    """
    """

    path = self.storeCreatePathFor(uuid)
    cache_path = os.path.join(path, "cache")

    if not os.path.exists(cache_path):
      os.mkdir(cache_path)

    if resource:
      resource_path = os.path.join(path, "resource")

      if not os.path.exists(resource_path):
        os.mkdir(resource_path)

    path = os.path.join(path, "repository")

    if not os.path.exists(path):
      os.mkdir(path)

    if resource:
      return resource_path

    return path

  # Interface methods follow

  def initialize(self, options):
    """
    """

    return None

  def establish(self, accountInfo):
    return accountInfo

  def unestablish(self, accountInfo):
    """ This does nothing.
    """

    return accountInfo

  def buildPathFor(self, uuid, storageHash, accountInfo, buildId, revision=None, create=False):
    objectPath = self.internalPathFor(uuid, revision=revision, buildId=buildId, build=True)

    if not os.path.exists(objectPath):
      if create:
        if not os.path.exists(os.path.realpath(os.path.join(objectPath, "..", ".."))):
          os.mkdir(os.path.realpath(os.path.join(objectPath, "..", "..")))
        if not os.path.exists(os.path.realpath(os.path.join(objectPath, ".."))):
          os.mkdir(os.path.realpath(os.path.join(objectPath, "..")))
        os.mkdir(objectPath)
        return objectPath

      return None

    return objectPath

  def resourcePathFor(self, uuid, create=False):
    objectPath = self.internalPathFor(uuid, resource=True)

    if not os.path.exists(objectPath):
      if create:
        return self.createPathFor(uuid, resource=True)

      return None

    return objectPath

  def repositoryPathFor(self, uuid, create=False):
    objectPath = self.internalPathFor(uuid)

    if not os.path.exists(objectPath):
      if create:
        return self.createPathFor(uuid)

      return None

    return objectPath

  def pathFor(self, storageHash, accountInfo, revision=None, create=False):
    uuid = storageHash['uuid']
    objectPath = self.internalPathFor(uuid, revision=revision)

    if not os.path.exists(objectPath) and create:
      if not os.path.exists(os.path.realpath(os.path.join(objectPath, "..", ".."))):
        os.mkdir(os.path.realpath(os.path.join(objectPath, "..", "..")))
      if not os.path.exists(os.path.realpath(os.path.join(objectPath, ".."))):
        os.mkdir(os.path.realpath(os.path.join(objectPath, "..")))
      os.mkdir(objectPath)

    if objectPath is None or not os.path.exists(objectPath):
      return None

    return objectPath

  def discoverNode(self, node):
    """
    """

    return

  def discover(self, uuid):
    """ Returns whether or not the given object exists on the network.
    """

    # Local stores do not discover. They are private stores.
    return []

  def isLocal(self, uuid):
    """ Returns whether or not the given object exists locally.
    """

    return False

  def clone(self, storageHash, accountInfo, path):
    """ Clones the given object to the given path.
    """

    uuid = storageHash['uuid']
    objectPath = self.internalPathFor(uuid)

    git = GitRepository(objectPath)
    data = git.clone(path)

    return data

  def retrieveHistory(self, storageHash, accountInfo, revision, path):
    """ Retrieve the repository log from the given hash.
    """

    uuid = storageHash['uuid']
    objectPath = self.internalPathFor(uuid)

    git = GitRepository(objectPath, revision=revision)
    data = git.history(path)

    return data

  def retrieveDirectory(self, storageHash, accountInfo, revision, path, buildId = None):
    """ Retrieve the given file path from the given hash.
    """

    uuid = storageHash['uuid']

    if buildId:
      buildPath = self.buildPathFor(uuid, storageHash, buildId, revision=revision)
      buildPath = os.path.join(buildPath, path)

      data = {"items": []}
      for filename in os.listdir(buildPath):
        filepath = os.path.join(buildPath, filename)
        filetype = "blob"
        if os.path.isdir(filepath):
          filetype = "tree"

        result = os.lstat(filepath)
        data['items'].append({
          "name": filename,
          "size": result.st_size,
          "type": filetype,
          "mode": result.st_mode
        })
    else:
      objectPath = self.internalPathFor(uuid)
      git = GitRepository(objectPath, revision=revision)
      data = git.retrieveDirectory(path)

    return data

  def retrieveFileStat(self, storageHash, accountInfo, revision, path, buildId = None):
    """ Retrieve the given file path from the given hash.
    """

    uuid = storageHash['uuid']

    if buildId:
      buildPath = self.buildPathFor(uuid, storageHash, buildId, revision=revision)

      filepath = os.path.join(buildPath, path)

      filetype = "blob"
      if os.path.isdir(filepath):
        filetype = "tree"

      result = os.lstat(filepath)
      data = {
        "name": filename,
        "size": result.st_size,
        "type": filetype,
        "mode": result.st_mode
      }
    else:
      objectPath = self.internalPathFor(uuid)
      git = GitRepository(objectPath, revision=revision)
      data = git.retrieveFileStat(path)

    return data

  def retrieveFile(self, storageHash, accountInfo, revision, path, buildId = None):
    """ Retrieve the given file path from the given hash.
    """

    uuid = storageHash['uuid']

    if buildId:
      buildPath = self.buildPathFor(uuid, storageHash, buildId, revision=revision)

      filepath = os.path.join(buildPath, path)
      data = open(filepath, "rb")
    else:
      objectPath = self.internalPathFor(uuid)
      git = GitRepository(objectPath, revision=revision)
      data = git.retrieveFile(path)

    return data

  def pull(self, uuid, storageHash, accountInfo):
    """ Pulls a copy of an existing object to the local store.
    """

    # Local objects are already local!

    return True

  def storeCreatePathFor(self, uuid):
    """ Creates a path to store an object with the given uuid.
    """

    if uuid is None:
      return None

    code = uuid
    code1 = code[0:2]
    code2 = code[2:4]

    # Objects are in: .occam/objects/<type>/<code[0:2]>/<code[2:4]>/full-code
    path = self.configuration.get('path', os.path.join(Config.root(), "objects"))

    if path is None:
      return None

    if not os.path.exists(path):
      os.mkdir(path)
    path = os.path.join(path, code1)
    if not os.path.exists(path):
      os.mkdir(path)
    path = os.path.join(path, code2)
    if not os.path.exists(path):
      os.mkdir(path)
    path = os.path.join(path, uuid)
    if not os.path.exists(path):
      os.mkdir(path)

    return path

  def storePathFor(self, uuid):
    """
    Returns the path the object of the given uuid would be stored.
    """

    code = uuid
    code1 = code[0:2]
    code2 = code[2:4]

    # Objects are in: .occam/objects/<type>/<code[0:2]>/<code[2:4]>/full-code
    object_path = self.configuration.get('path', os.path.join(Config.root(), "objects"))

    return os.path.realpath(os.path.join(object_path, code1, code2, uuid))

  def pushResource(self, uuid, revision, path, accountInfo):
    """ Writes the data at the given path to the store as a revision of the resource object.
    """

    storageHash = {
      'uuid': uuid,
      'revision': revision
    }

    if path.startswith(self.resourcePathFor(uuid)):
      return storageHash, None

    return None, None

  def pushBuild(self, uuid, revision, buildId, path, accountInfo):
    """
    Writes the data at the given path to the store as a build of the given
    object.
    """

    storageHash = {
      'uuid': uuid,
      'buildId': buildId,
      'revision': revision
    }

    # Determine the storage path
    storePath = self.buildPathFor(uuid, storageHash, accountInfo, buildId, revision, create=True)
    if storePath != path:
      # Copy the directory tree
      shutil.rmtree(storePath)
      shutil.copytree(path, storePath)

    return storageHash, None

  def push(self, uuid, path, revision, accountInfo):
    """ Writes a new object or revision to the store from the given path.
    """

    storageHash = {
      'uuid': uuid
    }

    # Place it in: <object path>/2-code/2-code/full-code/repository
    storePath = self.createPathFor(uuid)

    if path == storePath:
      # Someone is attempting to store an object from the object store!
      # Thus, it is already stored... do nothing. report success.
      return storageHash, None, None

    # Otherwise we'd have to copy the data in?
    return None, None, None

  def purge(self, uuid, storageHash, accountInfo):
    """
    Removes the object data from the store or marks it for deletion by a
    garbage collector.
    """

    # Delete the content

    return True

  def garbageCollect(self):
    """
    Notifies the storage backend that it should do some garbage collection.
    """

    # Local stores do not garbage collect.

    return True
