# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log import Log

from occam.manager import uses

from occam.accounts.manager    import AccountManager
from occam.permissions.manager import PermissionManager
from occam.objects.manager     import ObjectManager

from occam.commands.manager import command, option, argument

@command('permissions', 'list',
  category      = 'Access Management',
  documentation = "Lists the permissions available for the given object.")
@argument("object", type="object")
@uses(ObjectManager)
@uses(PermissionManager)
class AddCommand:
  def do(self):
    # Resolve the person
    obj = self.objects.resolve(self.options.object, person = self.person)
    if obj is None:
      Log.error("The object could not be found.")
      return -1

    objInfo = self.objects.infoFor(obj)
    objName = objInfo.get('name', 'unknown')

    # Default output
    ret = {'object': [], 'children': [], 'reviewLinks': []}

    # Get permissions
    records = self.permissions.retrieveAllRecords(uuid = obj.uuid, allPeople=True, addChildren=True)

    for record, person in records:
      item = {}
      for key, value in record._data.items():
        if key.startswith("can_"):
          key = key[4:]
          if value is None:
            item[key] = None
          else:
            item[key] = value == 1

      if record.person_object_id is not None:
        item['person'] = {
          'id': person._data.get('uid'),
          'name': person._data.get('name'),
          'revision': person._data.get('revision'),
          'type': person._data.get('object_type')
        }

        personInfo = self.objects.infoFor(self.objects.retrieve(uuid = item['person']['id']))
        item['person']['images'] = personInfo.get('images', [])

      if record.for_child_access == 1:
        ret['children'].append(item)
      else:
        ret['object'].append(item)

    # Get review links
    reviewLinks = self.permissions.retrieveReviewLinks(obj)

    for reviewLink in reviewLinks:
      ret['reviewLinks'].append({
        'id': reviewLink.id,
        'published': reviewLink.published.isoformat(),
        'object': {
          'id': obj.uuid,
          'revision': reviewLink.revision
        }
      })

    import json
    Log.output(json.dumps(ret))

    return 0
