mkdir -p /tmp

cp -r $1/$2 .
cp $1/ingest.py .

mkdir -p path2
rm -f path2/python2
ln -s /usr/bin/python2 path2/python 2>/dev/null
mkdir -p path3
rm -f path3/python2
ln -s /usr/bin/python3 path3/python 2>/dev/null

echo "{\"requirements\": "
if [ "$3" == "2" ]; then
  /bin/env PATH=$PWD/path2:$PATH python2 ingest.py $2 "${@:4}" > /dev/null 2> /dev/null
else
  /bin/env PATH=$PWD/path3:$PATH python3 ingest.py $2 "${@:4}" > /dev/null 2> /dev/null
fi
cat _output
echo "}"
