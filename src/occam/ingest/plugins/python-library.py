# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.ingest.manager import IngestManager, package

from occam.network.manager import NetworkManager

from occam.manager import uses

from occam.semver import Semver

import uuid as UUID
import os

@package("python-library")
@uses(NetworkManager)
class PythonLibrary:
  """ This aids in importing python packages into Occam for preservation.
  """

  CANONICAL_SOURCE = "https://pypi.python.org/pypi/%s"

  JSON_MIRRORS=[
    ("https://pypi.python.org/pypi/%s/json", "https://pypi.python.org/pypi/%s/%s/json",),
  ]

  def __queryMetadata(self, pythonLibraryName, version = None, pythonVersion = None):
    """ Retrieves the JSON PyPI metadata for the given python package that matches the given version and pythonVersion.

    Returns:
      None when the data cannot be found.
    """

    # Get the baseline
    metadata = self.__retrieveMetadata(pythonLibraryName, version)

    # Gather the versions
    releases = list(metadata.get('releases', {}).keys())

    # Sort the releases
    releases = Semver.sortVersionList(releases)

    # Go through the releases until we find something we can use
    for release in reversed(releases):
      if release != metadata.get('info', {}).get('version'):
        metadata = self.__retrieveMetadata(pythonLibraryName, release)

      requiresPython = metadata.get('info', {}).get('requires_python')
      if not pythonVersion or not requiresPython or Semver.resolveVersion(requiresPython, [pythonVersion]):
        return metadata

    return None

  def __retrieveMetadata(self, pythonLibraryName, version = None):
    """ Retrieves the JSON PyPI metadata for the given python package at the given package version.

    Returns:
      None when the data cannot be found.
    """

    for baseURL in PythonLibrary.JSON_MIRRORS:
      url = baseURL[0] % (pythonLibraryName)
      if version:
        url = baseURL[1] % (pythonLibraryName, version)
      data = self.network.getJSON(url)
      if data is not None:
        return data

    return None

  def idFor(self, pythonLibraryName, pythonVersion = None):
    """ Returns the uuid for a particular source package name.
    """

    sourceURL = PythonLibrary.CANONICAL_SOURCE % (pythonLibraryName.lower())

    if pythonVersion is not None:
      sourceURL = sourceURL + "?python=" + str(pythonVersion[0])

    return str(UUID.uuid5(UUID.NAMESPACE_URL, sourceURL))

  def query(self, pythonLibraryName, pythonVersion = "3"):
    """ Returns the uuid and initial metadata for the package.
    """

    sourceURL = PythonLibrary.CANONICAL_SOURCE % (pythonLibraryName)

    metadata = self.__queryMetadata(pythonLibraryName, version = None, pythonVersion = pythonVersion)

    if metadata is None:
      # Cannot find the metadata
      return None

    version = None

    # Gather version listing
    releases = list(metadata.get('info', {}).get('releases', {}).keys())

    # Use the 'latest' version listed in the metadata
    if version is None:
      version = metadata.get('info', {}).get('version')

    # If there is no listed latest, use the biggest version listed as a release
    if version is None and len(releases) > 0:
      version = Semver.sortVersionList(releases)[-1]

    # If we cannot determine which version to query, then we cannot find the package
    if version is None:
      return None

    pythonLibraryName = metadata['info']['name']

    uuid = self.uuidFor(pythonLibraryName, pythonVersion)

    objectName = pythonLibraryName
    if pythonVersion[0] == "2":
      objectName = objectName + "-2"

    PythonLibrary.Log.write("uuid: {}".format(uuid))

    # Get the URL of the desired release

    # First, gather a list of all downloadable distributions for the given version
    distributions = metadata.get('releases', {}).get(version, [])

    # And find the source distribution
    chosenDistribution = None
    for distribution in distributions:
      # TODO: handle python versions better
      if pythonVersion[0] == "3" and (distribution.get('python_version') == "cp36" or distribution.get('python_version') == "cp37"):
        url = distribution.get('url')
        if url:
          # If it has a URL, choose this one if the platform matches
          if "manylinux" in distribution.get('filename', distribution.get('url')):
            # And the architecture
            if "x86_64" in distribution.get('filename', distribution.get('url')):
              chosenDistribution = distribution
              break

      #if pythonVersion[0] == "2" and (distribution.get('python_version') == "cp26" or distribution.get('python_version') == "cp27"):

      if distribution.get('packagetype') == "sdist" and distribution.get('python_version') == "source":
        url = distribution.get('url')
        if url:
          # If it has a URL, choose this one
          chosenDistribution = distribution
          break

    # We could not find a download that we accept
    if chosenDistribution is None:
      return None

    url = chosenDistribution.get('url')

    filename, ext = os.path.splitext(os.path.basename(url))
    resourceType = "application/gzip"
    if ext == ".zip" or ext == ".whl":
      resourceType = "application/zip"
    elif ext == ".xz":
      resourceType = "application/x-xz"
    elif ext == ".bz2":
      resourceType = "application/x-bzip2"
    elif ext == ".lz":
      resourceType = "application/x-lzip"

    directoryName, ext = filename, ext
    if os.path.splitext(directoryName)[1] == ".tar":
      directoryName, ext = os.path.splitext(directoryName)

    isWheel = False

    if ext == ".whl":
      directoryName = directoryName + ext
      isWheel = True
    else:
      directoryName = directoryName + "/"

    # Create the object manifest that will determine the requirements
    #   by running an isolated setup.py
    objectInfo = {
      "id": uuid,
      "type": "python-library",
      "name": objectName,

      "source": sourceURL,

      "environment": "linux",
      "architecture": "x86-64",

      "run": {
        "install": [{
          "source": url,
          "type": resourceType,
          "name": "Python Source for %s %s" % (pythonLibraryName, version),
          "to": os.path.basename(url)
        }],
        "dependencies": [{
          "id": "cf31328a-5cfc-11e7-808f-f23c910a26c8",
          "type": "collection",
          "name": "build",
          "version": "1.0"
        },{
          "id": "1e30b1c6-65fe-11e7-8079-e06995729391",
          "type": "library",
          "name": "png",
          "version": "1.x"
        #},{
        #  "id": "52bca074-620e-11e7-a94d-e06995729391",
        #  "type": "library",
        #  "name": "freetype",
        #  "version": "2.x"
        #},{
        #  "name": "pkg-config",
        #  "type": "application",
        #  "id": "ddb0aee8-6117-11e7-ae16-e06995729391",
        #  "version": ">=0.29"
        },{
          "id": "ca9e45e4-4a16-11e7-8afa-f23c910a26c8",
          "version": ">4",
          "type": "compiler",
          "name": "g++"
        }],
        "command": ["/bin/sh", "{{ paths.mount }}/fakebuild.sh", "{{ paths.mount }}", directoryName, pythonVersion[0]]
      }
    }

    if not isWheel:
      objectInfo["run"]["install"][0]["actions"] = {"unpack": "."}

    if pythonVersion[0] == "2":
      objectInfo['run']['dependencies'].append({
        "id": "c33bab96-5cdd-11e7-8559-f23c910a26c8",
        "type": "language",
        "name": "python2",
        "version": ">=2.x",
        "lock": "build"
      })
    else:
      objectInfo['run']['dependencies'].append({
        "id": "efcfdd5c-41c2-11e7-82d6-f23c910a26c8",
        "type": "language",
        "name": "python",
        "version": ">=3.6.1",
        "lock": "build"
      })

    requiresPython = metadata.get('info', {}).get('requires_python')
    if requiresPython:
      objectInfo['run']['dependencies'][-1]['version'] += "," + requiresPython

    license = metadata.get('info', {}).get('license')
    if license:
      objectInfo['license'] = license

    summary = metadata.get('info', {}).get('summary')
    if summary:
      objectInfo['description'] = summary

    description = metadata.get('info', {}).get('description')
    if description:
      if summary:
        objectInfo['summary'] = summary
      objectInfo['description'] = description

    website = metadata.get('info', {}).get('home_page')
    if website:
      objectInfo['website'] = website

    author = metadata.get('info', {}).get('author')
    if author:
      objectInfo['authors'] = [author]

    # We need to parse the result of running this object to determine the requirements

    # TODO: check the md5 given by the metadata
    # TODO: use signatures?

    basePath = os.path.realpath(os.path.join(os.path.dirname(__file__), "..", "python-library"))

    buildScriptPath  = os.path.join(basePath, "fakebuild.sh")
    ingestPythonPath = os.path.join(basePath, "ingest.py")

    return objectInfo, {
      "files": [buildScriptPath, ingestPythonPath],
      "run": True,
      "tag": version
    }

  def report(self, runType, objectInfo, packageName, data, pythonVersion = "3"):
    """ Finishes the process.
    """

    if runType == "build":
      return None

    packageName = objectInfo.get('name')

    import json, re

    setupArguments = {}
    print(data)

    try:
      setupArguments = json.loads(data)
    except:
      # Cannot parse the output
      pass

    requirements = setupArguments["requirements"]

    # Clone the existing object
    # We will modify it with new requirements and dependencies
    newObjectInfo = objectInfo.copy()

    # First, we need to see if there are any dependencies that are required
    # to do an installation
    setupDependencies = False

    if requirements is None:
      # Failed to parse
      # TODO: try an earlier version
      return None

    if isinstance(requirements, dict):
      if "setup_requires" in requirements:
        # Add the requirements to the new form of object and have it re-run eventually
        setupDependencies = True
        requirements = requirements["setup_requires"]

    # We will use this regular expression to parse python versions
    splitter = re.compile(r"^([^><=~!]+)(.*)$")

    # Create the object manifest that will determine the requirements
    #   by running an isolated setup.py
    section = 'run'

    basePath = os.path.realpath(os.path.join(os.path.dirname(__file__), "..", "python-library"))
    buildScriptPath  = os.path.join(basePath, "fakebuild.sh")
    ingestPythonPath = os.path.join(basePath, "ingest.py")
    filesList = [buildScriptPath, ingestPythonPath]

    if not setupDependencies:
      buildScriptPath = os.path.join(basePath, "build.sh")
      filesList = [buildScriptPath]

      section = 'build'
      newObjectInfo['build'] = newObjectInfo['run']
      directoryName = newObjectInfo['build']['install'][0]['to']
      directoryName, ext = os.path.splitext(directoryName)
      if os.path.splitext(directoryName)[1] == ".tar":
        directoryName, ext = os.path.splitext(directoryName)

      isWheel = False

      if ext == ".whl":
        directoryName = directoryName + ext
        isWheel = True
      else:
        directoryName = directoryName + "/"

      newObjectInfo['build']['command'] = ["/bin/sh", "{{ paths.mount }}/build.sh", directoryName, pythonVersion[0]]
      del newObjectInfo['run']

      newObjectInfo['build']['dependencies'] = newObjectInfo['build'].get('dependencies', [])
    else:
      newObjectInfo['run']['command'].append("--ignore-setup-dependencies")

    newObjectInfo['init'] = {
      "link": [
        {
          "source": "usr",
          "to":     "/usr"
        }
      ]
    }

    ingestRequests = []

    if requirements:
      for request in requirements:
        result = splitter.match(request)
        if result is None:
          # TODO: error? it is a malformed requirement
          continue

        name    = result.group(1)
        version = result.group(2)

        metadata = self.__retrieveMetadata(name)
        name = metadata['info']['name']

        if version is None or version == "":
          version = ">0"

        uuid = self.uuidFor(name, pythonVersion[0])
        subName = name
        if pythonVersion[0] == "2":
          subName = subName + "-2"

        dependencyInfo = {
          "id": uuid,
          "name": subName,
          "type": "python-library",
          "version": version
        }

        newObjectInfo['dependencies'] = newObjectInfo.get('dependencies', [])
        newObjectInfo['dependencies'].append(dependencyInfo)

        ingestRequests.append({
          "packageType": "python-library",
          "packageName": name,
          "pythonVersion": pythonVersion
        })

    basePath = os.path.realpath(os.path.join(os.path.dirname(__file__), "..", "python-library"))

    report = {
      "files": filesList,
      "ingest": ingestRequests
    }

    report[section] = True

    return newObjectInfo, report

  def craft(self):
    """ Creates an Occam object from the given package resource.
    """

    # For Python packages, they use a build system written in python where the
    # packages are described in python code. This is unfortunate since the
    # determination of requirements/dependencies has to be done by executing
    # that python configuration.

    # Therefore, the first step (if the python package is not a wheel, see
    # below) is to create an initial object to download the python resource
    # and run the setup.py a particular way to parse the requirements.

    # If the python package is not the obsolete "egg" format, and is instead
    # a "wheel" format, the requirements are already listed and the code is
    # already compiled. The "wheel" packages are like binary distributions
    # where "egg" packages are source.

    # The second step (or first, for "wheel" packages) is to then use the
    # dependency list to create the object with a "build" and "init"
    # section. The "build" process will run the setup.py and then install
    # the files to the built object.

    # We then need to recursively gather the required packages.
